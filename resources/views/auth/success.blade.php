@extends('layouts.auth')

@section('title')
    {{ $title }} |
@endsection

@section('content')
    <div id="app" class="main-content-boxed">

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="bg-body-dark bg-pattern">
                <div class="row mx-0 justify-content-center">
                    <div class="hero-static col-lg-6 col-xl-4">
                        <div class="content content-full overflow-hidden">
                            <!-- Header -->
                            <div class="py-30 text-center">
                                <a class="link-effect font-w700" href="/">
                                    <img src="{{ url('/media/images/qt-icon.png') }}" alt="QueTap" class="logo-icon">
                                    <span class="d-inline-block">
                                        <span class="font-size-xl text-primary">UETAP PARTNER</span>
                                    </span>
                                </a>
                                <h1 class="h4 font-w700 mt-30 mb-10">{{ $title }}</h1>
                                <h2 class="h5 font-w400 text-muted mb-0">{{ 'Hi, ' . $partner->first_name . '. ' . $message }}</h2>
                            </div>
                            <!-- END Header -->

                            <div class="text-center">
                                <img src="{{ url('/media/images/Rocket-pana.png') }}" alt="" class="w-100">

                                <a class="link-effect font-w700" href="/">
                                    <span class="d-inline-block">
                                        <span class="font-size-xl text-primary"><i class="fa fa-arrow-left"></i> Return to Homepage</span>
                                    </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
@endsection