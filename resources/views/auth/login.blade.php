@extends('layouts.auth')

@section('title')
    Sign In |
@endsection

@section('content')
    <div id="app" class="main-content-boxed">

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="bg-body-dark bg-pattern">
                <div class="row mx-0 justify-content-center">
                    <div class="hero-static col-lg-6 col-xl-4">
                        <div class="content content-full overflow-hidden">
                            <!-- Header -->
                            <div class="py-30 text-center">
                                <a class="link-effect font-w700" href="/">
                                    <img src="{{ url('media/images/qt-icon.png') }}" alt="QueTap" class="logo-icon">
                                    <span class="d-inline-block">
                                        <span class="font-size-xl text-primary">UETAP</span>
                                    </span>
                                </a>
                                <h1 class="h4 font-w700 mt-30 mb-10">Welcome to Your Dashboard</h1>
                                <h2 class="h5 font-w400 text-muted mb-0">It’s a great day today!</h2>
                            </div>
                            <!-- END Header -->

                            <form class="js-validation-signin " id="loginform" method="post" action="{{ route('login') }}">
                                @csrf
                                <div class="block block-themed block-rounded block-shadow">
                                    <div class="block-header bg-primary">
                                        <h3 class="block-title">Please Sign In</h3>
                                    </div>
                                    <div class="block-content">
                                        @error('info')
                                            <div class="alert alert-info mb-0">
                                                <i class="fa fa-info-circle"></i> {{ $message }}
                                            </div>
                                        @enderror

                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material {{ !old('email') ? 'floating' : ''}}">
                                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
                                                    
                                                    <label for="email">Email</label>

                                                    @error('email')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material floating">
                                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                                    <label for="password">Password</label>

                                                    @error('password')
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $message }}</strong>
                                                        </span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            {{-- <div class="col-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label class="custom-control-label" for="remember">Remember Me</label>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="form-group text-center">
                                            <button type="submit" class="btn btn-alt-primary">
                                                <i class="si si-login mr-10"></i> Sign In
                                            </button>
                                            {{-- @if (Route::has('password.request'))
                                                <div class="mt-30">
                                                    <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="{{ route('password.request') }}">
                                                        <i class="fa fa-warning mr-5"></i> Forgot Your Password?
                                                    </a>
                                                </div>
                                            @endif --}}
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END Sign In Form -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
@endsection