@extends('layouts.auth')

@section('title')
    Partner's Onboarding |
@endsection

@section('css_after')
<link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css"> --}}
<link rel="stylesheet" href="{{ asset('js/plugins/bootstrap-select/css/bootstrap-select.min.css') }}">

<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
      height: 400px;
      width: 100%;
    }
  </style>
@endsection

@section('js_after')
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/js/bootstrap-select.min.js"></script> --}}
    {{-- <script src="https://unpkg.com/@popperjs/core@2"></script> --}}

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>

    <script src="{{ asset('js/plugins/bootstrap-select/js/bootstrap-select.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            var success = $('#app').data('success');

            if(success) {
                swal.fire({
                    title: "Success!!",
                    text: "Thank you for sending us your details. We will update you once we verify all your details. You may receive a call from us for our verifcation.",
                    icon: "success",
                    showCancelButton: !1,
                    buttonsStyling: !1,
                    customClass: {
                        confirmButton: "btn btn-alt-success m-5",
                        cancelButton: "btn btn-alt-danger m-5",
                        input: "form-control"
                    },
                    html: !1,
                    allowOutsideClick: false,
                })
                .then(() => {
                    window.location.href="/"
                });
            }

            $('#industry').selectpicker({
                maxOptions: 1
            });
            $('#specialization_select').selectpicker({
                liveSearch: true
            });
            $('#insurance_select').selectpicker({
                liveSearch: true
            });

            $('#industry').on('change', (e) => {
                if(e.target.value == 'Healthcare' || e.target.value == 'Veterinary') {
                    $('#other-details').removeClass('d-none');
                } else {
                    $('#other-details').addClass('d-none');
                }
            })

            $('#specialization_select').on('change', (e) => {
                $('#specialization').val($('#specialization_select').val())
            })

            $('#insurance_select').on('change', (e) => {
                $('#insurance').val($('#insurance_select').val())
            })
        });

        function agreeToTerms() {
            $('#signup-terms').prop('checked', true)
        }

        function addLocation() {
            swal.fire({
                title: "Add Another Location",
                text: "To add another location, please email at support@quetap.ph. Thank you.",
                icon: "warning",
                showCancelButton: !1,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                html: !1,
                allowOutsideClick: false,
            })
        }

        function initMap() {
            var myLatlng = {lat: 10.3157, lng: 123.8854};

            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 14, center: myLatlng});

            // Create the initial InfoWindow.
            var infoWindow = new google.maps.InfoWindow(
                {content: 'Click the map to get exact location of your business address', position: myLatlng});
            infoWindow.open(map);

            // Configure the click listener.
            map.addListener('click', function(mapsMouseEvent) {
                // Close the current InfoWindow.
                infoWindow.close();

                // Create a new InfoWindow.
                infoWindow = new google.maps.InfoWindow({position: mapsMouseEvent.latLng});
                infoWindow.setContent(mapsMouseEvent.latLng.toString());
                infoWindow.open(map);

                // for (let i = 0; i < markers.length; i++) {
                //     markers[i].setMap(null);
                // }

                // var image = 'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png';
                // var beachMarker = new google.maps.Marker({
                //     position: mapsMouseEvent.latLng,
                //     map: map,
                //     icon: image
                // });

                $('input[name="lat_long"]').val(mapsMouseEvent.latLng.toString())
            });
        }
    </script>
    <script defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDs-0wDhC19InbHxIWRpQFoiCjpxuLC85Q&callback=initMap">
    </script>

@endsection

@section('content')
    <div id="app" class="main-content-boxed" data-success="{{ Session::get('success') }}">

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="bg-body-dark bg-pattern">
                <div class="row mx-0 justify-content-center">
                    <div class="hero-static col-lg-6 col-xl-6">
                        <div class="content content-full overflow-hidden">
                            <!-- Header -->
                            <div class="py-30 text-center">
                                <a class="link-effect font-w700" href="/">
                                    <img src="{{ url('media/images/qt-icon.png') }}" alt="QueTap" class="logo-icon">
                                    <span class="d-inline-block">
                                        <span class="font-size-xl text-primary">UETAP PARTNER</span>
                                    </span>
                                </a>
                                <h1 class="h4 font-w700 mt-30 mb-10">Welcome to QueTap, {{ $partner->first_name }}!</h1>
                                <h2 class="h5 font-w400 text-muted mb-0">Complete your information below.</h2>
                            </div>
                            <!-- END Header -->

                            @if ($errors->any())
                                <div class="alert alert-danger alert-dismissable" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">×</span>
                                    </button>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            <form class="js-validation-signin " id="loginform" method="post" action="{{ route('partners-onboarding-post', ['id' => $id, 'token' => $token]) }}" enctype="multipart/form-data">
                                @csrf
                                <div class="block block-themed block-rounded block-shadow">
                                    <div class="block-header bg-primary">
                                        <h3 class="block-title">Partner's Details</h3>
                                    </div>
                                    <div class="block-content">
                                        @error('info')
                                            <div class="alert alert-info mb-0">
                                                <i class="fa fa-info-circle"></i> {{ $message }}
                                            </div>
                                        @enderror

                                        <input type="hidden" name="partner_id" value="{{ $partner->id }}">
                                        <input type="hidden" name="token" value="{{ $partner->token }}">

                                        <div class="form-group row mt-2 mb-0">
                                            <div class="col-sm-12 col-md-4 push text-center">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="sole-proprietor" value="sole-proprietor" name="business_type" class="custom-control-input" {{ old('business_type')=="sole-proprietor" ? 'checked='.'"'.'checked'.'"' : '' }} required> 
                                                    <label for="sole-proprietor" class="custom-control-label">Sole Proprietor</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 push text-center">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="partnership" value="partnership" name="business_type" class="custom-control-input" {{ old('business_type')=="partnership" ? 'checked='.'"'.'checked'.'"' : '' }} required> 
                                                    <label for="partnership" class="custom-control-label">Partnership</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-4 push text-center">
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" id="corporation" value="corporation" name="business_type" class="custom-control-input" {{ old('business_type')=="corporation" ? 'checked='.'"'.'checked'.'"' : '' }} required> 
                                                    <label for="corporation" class="custom-control-label">Corporation</label>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input id="business_name" type="text" class="form-control @error('business_name') is-invalid @enderror" name="business_name" value="{{ old('business_name') }}" required>
                                                    <label for="business_name">Registered Proprietor's / Business Name <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-7">
                                                <div class="form-material">
                                                    <input id="representative" type="text" class="form-control @error('representative') is-invalid @enderror" name="representative" value="{{ old('representative') }}" required>
                                                    <label for="representative">Authorized Representative <small class="required">*</small></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-5">
                                                <div class="form-material">
                                                    <input id="position" type="text" class="form-control @error('position') is-invalid @enderror" name="position" value="{{ old('position') }}" required>
                                                    <label for="position">Position <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input id="contact_no" type="text" class="form-control @error('contact_no') is-invalid @enderror" name="contact_no" value="{{ (old('contact_no'))? old('contact_no') : $partner->first_name }}" required>
                                                    <label for="contact_no">Contact Number <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material">
                                                    <label>Registered Business Address</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-12">
                                                <div class="form-material">
                                                    <input id="business_address_street" type="text" class="form-control @error('business_address_street') is-invalid @enderror" name="business_address_street" value="{{ old('business_address_street') }}" required>
                                                    <label for="business_address_street">Floor No./Building Name/Street/Barangay <small class="required">*</small></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 col-md-5">
                                                <div class="form-material">
                                                    <input type="text" name="business_address_province" id="business_address_province" class="form-control" value="{{ old('business_address_province') }}" required>
                                                    {{-- <select name="business_address_province" id="business_address_province" class="form-control" required>
                                                        <option value="" selected disabled>Select Province</option>
                                                        <option value="Cebu" @if (old('business_address_province') == "Cebu") {{ 'selected' }} @endif>Cebu</option>
                                                    </select> --}}
                                                    <label for="business_address_province">Province <small class="required">*</small></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-5">
                                                <div class="form-material">
                                                    <input type="text" name="business_address_city" id="business_address_city" value="{{ old('business_address_city') }}" class="form-control" required>
                                                    {{-- <select name="business_address_city" id="business_address_city" class="form-control" required>
                                                        <option value="" selected disabled>Select City</option>
                                                        <option value="Cebu City" @if (old('business_address_city') == "Cebu City") {{ 'selected' }} @endif>Cebu City</option>
                                                        <option value="Cebu City" @if (old('business_address_city') == "Mandaue City") {{ 'selected' }} @endif>Mandaue City</option>
                                                        <option value="Cebu City" @if (old('business_address_city') == "Search Results
                                                        Web results
                                                        
                                                        Lapu-Lapu City") {{ 'selected' }} @endif>Search Results
                                                            Web results
                                                            
                                                            Lapu-Lapu City</option>
                                                    </select> --}}
                                                    <label for="business_address_city">City <small class="required">*</small></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-12 col-md-2">
                                                <div class="form-material">
                                                    <input id="business_address_zip_code" type="text" class="form-control @error('business_address_zip_code') is-invalid @enderror" name="business_address_zip_code" value="{{ old('business_address_zip_code') }}" required>
                                                    <label for="business_address_zip_code">Zip code <small class="required">*</small></label>
                                                </div>
                                            </div>

                                            <div class="col-12 mt-4">
                                                <div id="map"></div>

                                                <input type="hidden" name="lat_long" value="{{ old('lat_long') }}">
                                            </div>

                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12 text-center">
                                                <button type="button" class="btn btn-sm btn-link" onclick="addLocation()">Add Other Location</button>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <label for="industry">Industry <small class="required">*</small></label>
                                                <select name="industry" id="industry" class="form-control" required>
                                                    <option value="" selected disabled>Select Industry</option>

                                                    @foreach ($industries as $industry)
                                                        <option value="{{ $industry->name }}" @if (old('industry') == $industry->name) {{ 'selected' }} @endif>{{ $industry->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row d-none" id="other-details">
                                            <div class="col-sm-12 col-md-6">
                                                <input type="hidden" name="specialization" id="specialization">
                                                <label for="specialization_select">Specialization</label>
                                                <select name="specialization_select" id="specialization_select" class="form-control" multiple size="10">
                                                    <option value="" disabled>Select all that apply</option>

                                                    @foreach ($specializations as $specialization)
                                                        <option value="{{ $specialization->name }}">{{ $specialization->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-sm-12 col-md-6">
                                                <input type="hidden" name="insurance" id="insurance">
                                                <label for="insurance_select">Insurance</label>
                                                <select name="insurance_select" id="insurance_select" class="form-control"  multiple size="10">
                                                    <option value="" disabled>Select all that apply</option>

                                                    @foreach ($insurances as $insurance)
                                                        <option value="{{ $insurance->name }}">{{ $insurance->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <textarea class="form-control" name="additional_details" id="" cols="30" rows="5">{{ old('additional_details') }}</textarea>
                                                    <label for="insurance">Additional Details</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12">
                                                <div class="form-material">
                                                    <label>Required Documents</label>
                                                </div>
                                            </div>
                                        </div>
                                        <small><i>Required upon registration:</i></small>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input type="file" name="logo" id="logo" accept=".jpg, .png, .pdf">
                                                    <label for="logo">1x1 ID Picture (with collar and with plain background) for industries with profession</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input type="file" name="business_permit" id="business_permit" accept=".jpg, .png, .pdf">
                                                    <label for="business_permit">Business Permit or PRC ID</label>
                                                </div>
                                            </div>
                                        </div>
                                        <small><i>Could be submitted later:</i></small>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input type="file" name="dti_sec_cert" id="dti_sec_cert" accept=".jpg, .png, .pdf">
                                                    <label for="dti_sec_cert">DTI or SEC Certificate (if applicable)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-sm-12">
                                                <div class="form-material">
                                                    <input type="file" name="bir_cert" id="bir_cert" accept=".jpg, .png, .pdf">
                                                    <label for="bir_cert">BIR 2303 Form (Certificate or Registration)</label>
                                                </div>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row mt-2 mb-0">
                                            <div class="col-sm-12 push">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" id="onboarding_terms" name="onboarding_terms" class="custom-control-input" {{ old('business_type')=="5" ? 'checked='.'"'.'checked'.'"' : '' }}> 
                                                    <label for="onboarding_terms" class="custom-control-label">I hereby certify that the above information is true and correct and hereby authorize the release of any credit information from the above named references pertaining to my/our credit and financial responsibilities to whom the application is made.</label>
                                                </div>
                                            </div> 
                                        </div>


                                        <div class="form-group text-center mb-4">
                                            <button type="submit" class="btn btn-alt-primary">
                                                <i class="fa fa-paper-plane mr-10"></i> Submit
                                            </button>
                                        </div>
                                    </div>

                                    {{-- <div class="block-content bg-body-light">
                                        <div class="form-group text-center">
                                            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="#" data-toggle="modal" data-target="#modal-terms">
                                                <i class="fa fa-book text-muted mr-5"></i> Read Terms
                                            </a>
                                            <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="http://partners.quetap.ph/">
                                                <i class="fa fa-user text-muted mr-5"></i> Sign In
                                            </a>
                                        </div>
                                    </div> --}}

                                </div>
                            </form>
                            <!-- END Sign In Form -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
@endsection