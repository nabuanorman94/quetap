@extends('layouts.backend')

@section('title')
    Insurances | 
@endsection

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>

    <script>
        $(document).ready(function(){

            setTimeout(() => {
                $(".alert").fadeOut(1000);
            }, 4000)

            // Clear all form modals
            clearInsuranceForm();

            setTimeout(() => {
                $('.table').removeClass('d-none');
                $('.table-loading').addClass('d-none');
            }, 1000)
        });

        $("#add-insurance-modal").on("hidden.bs.modal", function () {
            clearInsuranceForm();
        });

        function addInsurance() {
            $('#add-insurance-modal').modal('show');
        }

        function editInsurance(insurance) {
            $('input[name="data_id"]').val(insurance.id);
            $('input[name="name"]').val(insurance.name);
            $('#add-insurance-modal').modal('show');
        }

        function removeInsurance(id) {
            swal.fire({
            title: "Delete Insurance?",
            text: "You will not be able to recover action!",
            icon: "question",
            showCancelButton: !0,
            buttonsStyling: !1,
            customClass: {
                confirmButton: "btn btn-alt-success m-5",
                cancelButton: "btn btn-alt-danger m-5",
                input: "form-control"
            },
            confirmButtonText: "Yes",
            html: !1,
            })
            .then((result) => {
            if(result.value) {
                $.ajax({
                    url: "{{ route('action-remove') }}",
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                        model: 'Insurance'
                    }
                    // context: document.body
                }).done(function() {
                    swal.fire({
                        title: "Success!",
                        text: "Please reload your page.",
                        icon: "success",
                        buttonsStyling: !1,
                        customClass: {
                            confirmButton: "btn btn-alt-success m-5",
                            input: "form-control"
                        },
                        confirmButtonText: "Reload now",
                        allowOutsideClick: false,
                        html: !1,
                    })
                    .then((result) => {
                        if(result.value) {
                            window.location.reload(true)
                        }
                    })
                });
            }
            })
        }

        function clearInsuranceForm() {
            $('input[name="data_id"]').val('');
            $('#name').val('');
        }
    </script>
@endsection

@section('content')
    <div id="app" class="content">
        <h2 class="content-heading"><i class="fa fa-database"></i> Manage Data <small>Insurances</small></h2>

        @include('common.simple-alerts')

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Insurances</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" onclick="addInsurance()">
                        <i class="fa fa-plus"></i> Add Insurance
                    </button>
                </div>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full d-none">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">#</th>
                            <th>Name</th>
                            <th style="width: 15%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($insurances as $insurance)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="font-w600">
                                <a href="javascript:void(0)">{{ $insurance->name }}</a>
                            </td>
                            <td class="text-center">
                                <button type="button" class="btn btn-sm btn-alt-info" onclick="editInsurance({{$insurance}})"><i class="fa fa-pencil"></i> Edit</button>
                                <button type="button" class="btn btn-sm btn-alt-secondary" onclick="removeInsurance({{$insurance->id}})"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <p class="table-loading text-center"><i>Loading table data...</i></p>
            </div>
        </div>
    </div>

    <div class="modal fade" id="add-insurance-modal" tabindex="-1" role="dialog" aria-labelledby="add-insurance-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent">
                    <div class="block-header bg-primary">
                        <h3 class="block-title">Add Insurance</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <form action="{{ route('action-create') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="hidden" name="data_id">
                            <input type="hidden" name="model" value="Insurance">
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label for="name">Insurance Name</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group mt-3 text-center">
                                <button type="submit" class="btn btn-alt-primary">
                                    <i class="fa fa-floppy-o"></i> Save
                                </button>
                                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                    <i class="fa fa-times"></i> Cancel
                                </button>
                            </div>
    
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection