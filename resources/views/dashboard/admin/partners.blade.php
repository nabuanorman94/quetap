@extends('layouts.backend')

@section('title')
    Partners | 
@endsection

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>

    <script>
        $(document).ready(function(){
            $("#show-partner-details-modal").on("hidden.bs.modal", function () {
                clearPartnerModal();
            });

            setTimeout(() => {
                $('.table').removeClass('d-none');
                $('.table-loading').addClass('d-none');
            }, 1000)
        });

        function clearPartnerModal() {
            $('#partner_name').html('');
            $('#partner_email').html('');
            $('#partner_password').val('');
            $('#partner_onboarded').html('');
            $('#partner_verified').html('');

            $('.show_on_partner_details').hide();

            $('#partner_details_business_type').html('');
            $('#partner_details_business_name').html('');
            $('#partner_details_business_address').html('');
            $('#partner_details_industry').html('');
            $('#partner_details_specialization').html('');
            $('#partner_details_insurance').html('');
            $('#partner_details_representative').html('');
            $('#partner_details_position').html('');
            $('#partner_details_contact_no').html('');

            $('#logo').attr('src', '');
            $('#business_permit').attr('src', '');
            $('#bir_cert').attr('src', '');
            $('#dti_sec_cert').attr('src', '');

            $('#logo_link').html('');
            $('#business_permit_link').html('');
            $('#bir_cert_link').html('');
            $('#dti_sec_cert_link').html('');
        }

        function showDetails(partner) {
            var my_onboarded = (partner.onboarded)? 'Yes' : 'No';
            var my_verified = (partner.verified)? 'Verified' : 'Not Verified';
            var middle_name = (partner.middle_name)? partner.middle_name : ' ';

            $('#partner_name').html(partner.first_name + ' ' + middle_name + ' ' + partner.last_name);
            $('#partner_email').html(partner.email);
            $('#partner_password').val(partner.password);
            $('#partner_onboarded').html(my_onboarded);
            $('#partner_verified').html(my_verified);

            if(partner.partner_details != null) {
                $('.show_on_partner_details').show();

                $('#partner_details_business_type').html(partner.partner_details.business_type);
                $('#partner_details_business_name').html(partner.partner_details.business_name);
                $('#partner_details_business_address').html(partner.partner_details.business_address_street + ', ' + partner.partner_details.business_address_city + ', ' + partner.partner_details.business_address_province + ', ' + partner.partner_details.business_address_zip_code + ', Philippines');
                $('#partner_details_industry').html(partner.partner_details.industry);
                $('#partner_details_specialization').html(partner.partner_details.specialization);
                $('#partner_details_insurance').html(partner.partner_details.insurance);
                $('#partner_details_representative').html(partner.partner_details.representative);
                $('#partner_details_position').html(partner.partner_details.position);
                $('#partner_details_contact_no').html(partner.partner_details.contact_no);
                $('#partner_details_lat_long').html(partner.partner_details.lat_long)
                $('#partner_details_additional').html(partner.partner_details.additional_details)

                $('#logo').attr('src', partner.partner_details.logo);
                $('#business_permit').attr('src', partner.partner_details.business_permit);
                $('#bir_cert').attr('src', partner.partner_details.bir_cert);
                $('#dti_sec_cert').attr('src', partner.partner_details.dti_sec_cert);

                $('#logo_link').html(partner.partner_details.logo);
                $('#business_permit_link').html(partner.partner_details.business_permit);
                $('#bir_cert_link').html(partner.partner_details.bir_cert);
                $('#dti_sec_cert_link').html(partner.partner_details.dti_sec_cert);

                $('#logo_link_target').attr('href', partner.partner_details.logo);
                $('#business_permit_link_target').attr('href', partner.partner_details.business_permit);
                $('#bir_cert_link_target').attr('href', partner.partner_details.bir_cert);
                $('#dti_sec_cert_link_target').attr('href', partner.partner_details.dti_sec_cert);
            } else {
                $('.show_on_partner_details').hide()
            }
            
            // setTimeout(() => {
            //     $('#show-partner-details-modal').modal('show');
            // }, 500)
            $('#show-partner-details-modal').modal('show');
        }

        function showPassword(e) {
            if($('#partner_password').attr('type') == 'password') {
                $('#partner_password').attr('type', 'text');
                $(e).html('Hide');
            } else {
                $('#partner_password').attr('type', 'password');
                $(e).html('Show');
            }
        }

        function removePartner(id) {
            swal.fire({
            title: "Delete Partner?",
            text: "You will not be able to recover action!",
            icon: "question",
            showCancelButton: !0,
            buttonsStyling: !1,
            customClass: {
                confirmButton: "btn btn-alt-success m-5",
                cancelButton: "btn btn-alt-danger m-5",
                input: "form-control"
            },
            confirmButtonText: "Yes",
            html: !1,
        })
        .then((result) => {
            if(result.value) {
                $.ajax({
                    url: "{{ route('action-remove') }}",
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        id: id,
                        model: 'Partner'
                    }
                    // context: document.body
                }).done(function() {
                    swal.fire({
                        title: "Success!",
                        text: "Please reload your page.",
                        icon: "success",
                        buttonsStyling: !1,
                        customClass: {
                            confirmButton: "btn btn-alt-success m-5",
                            input: "form-control"
                        },
                        confirmButtonText: "Reload now",
                        allowOutsideClick: false,
                        html: !1,
                    })
                    .then((result) => {
                        if(result.value) {
                            window.location.reload(true)
                        }
                    })
                });
            }
        })
        }

        function onboard(base_url, id, token) {
            window.open(
                base_url+'/auth/partner/'+id+'/onboarding/'+token,
                '_blank'
            );
        }
    </script>
@endsection

@section('content')
    <div id="app" class="content">
        <h2 class="content-heading"><i class="fa fa-handshake-o"></i> Partners</h2>

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Partners List</h3>
            </div>
            <div class="block-content block-content-full">
                <!-- DataTables init on table by adding .js-dataTable-full class, functionality is initialized in js/pages/tables_datatables.js -->
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full d-none">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">#</th>
                            <th>Name</th>
                            <th class="d-none d-sm-table-cell" style="width: 21%;">Email Address</th>
                            <th style="width: 11%;" class="text-center">Contact Number</th>
                            <th style="width: 10%;" class="text-center">Available Between</th>
                            <th style="width: 6%;" class="text-center">Onboarded</th>
                            <th style="width: 5%;" class="text-center">Verified</th>
                            <th style="width: 8%;" class="text-center">Date</th>
                            <th style="width: 14%;" class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($partners as $partner)
                        <tr>
                            <td class="text-center">{{ $loop->iteration }}</td>
                            <td class="font-w600">
                                <a href="javascript:void(0)" onclick="showDetails({{$partner}})">{{ $partner->first_name.' '.$partner->middle_name.' '.$partner->last_name }}</a>
                            </td>
                            <td class="d-none d-sm-table-cell">
                                {{ $partner->email }}
                            </td>
                            <td class="text-center">
                                {{ $partner->contact_no }}
                            </td>
                            <td class="text-center">
                                <small>{{ $partner->time_from . ' - ' . $partner->time_to }}</small>
                            </td>
                            <td class="text-center">
                                {{ ($partner->onboarded)? 'Yes' : 'No' }}
                            </td>
                            <td class="text-center">
                                {{ ($partner->verified)? 'Yes' : 'No' }}
                            </td>
                            <td class="text-center">
                                {{ $partner->created_at->format('Y-m-d') }}
                            </td>
                            <td class="text-center">
                                {{-- @if (!$partner->verified)
                                    <button type="button" class="btn btn-sm btn-alt-success"><i class="fa fa-check"></i> Verify</button>
                                @endif --}}
                                <button type="button" class="btn btn-sm btn-alt-success" onclick="onboard('{{Config::get('app.url')}}', {{$partner->id}}, '{{$partner->token}}')" @if($partner->onboarded) disabled @endif><i class="fa fa-check"></i> Onboard</button>
                                <button type="button" class="btn btn-sm btn-alt-secondary" onclick="removePartner({{$partner->id}})"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <p class="table-loading text-center"><i>Loading table data...</i></p>
            </div>
        </div>
    </div>

    <div class="modal fade" id="show-partner-details-modal" tabindex="-1" role="dialog" aria-labelledby="show-partner-details-modal" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg modal-dialog-popout" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent">
                    <div class="block-header bg-primary">
                        <h3 class="block-title">Partner Details</h3>
                        <div class="block-options">
                            <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                                <i class="si si-close"></i>
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="mb-0">Name: <b><i id="partner_name"></i></b></p>
                                <p class="mb-0">Email: <b><i id="partner_email"></i></b></p>
                                <p class="mb-0">Password: <input type="password" id="partner_password"> <button class="btn btn-sm btn-warning" onclick="showPassword(this)">Show</button></p>
                                <p class="mb-0">Onboarded: <b><i id="partner_onboarded"></i></b></p>
                                <p class="mb-3">Status: <b><i id="partner_verified"></i></b></p>
                            </div>
                        </div>
                        <hr class="show_on_partner_details">
                        <div class="row show_on_partner_details">
                            <div class="col-sm-12 col-md-6">
                                <p class="mb-0">Business Type: <b><i id="partner_details_business_type"></i></b></p>
                                <p class="mb-0">Business Name: <b><i id="partner_details_business_name"></i></b></p>
                                <p class="mb-0">Business Address: <b><i id="partner_details_business_address"></i></b></p>
                                <p class="mb-0">Industry: <b><i id="partner_details_industry"></i></b></p>
                                <p class="mb-0">Specialization: <b><i id="partner_details_specialization"></i></b></p>
                                <p class="mb-2">Insurance: <b><i id="partner_details_insurance"></i></b></p>
                            </div>
                            <div class="col-sm-12 col-md-6">
                                <p class="mb-0">Map Pin: <b><i id="partner_details_lat_long"></i></b></p>
                                <p class="mb-0">Additional Info: <b><i id="partner_details_additional"></i></b></p>
                                <hr>
                                <p class="mb-0">Representative: <b><i id="partner_details_representative"></i></b></p>
                                <p class="mb-0">Position: <b><i id="partner_details_position"></i></b></p>
                                <p>Contact Number: <b><i id="partner_details_contact_no"></i></b></p>
                            </div>
                        </div>
                        <hr class="show_on_partner_details">
                        <div class="row show_on_partner_details">
                            <div class="col-sm-12 col-md-3">
                                <p>1x1 ID Picture (with collar and with plain background) for industries with profession:</p>
                                <a href="" id="logo_link_target" target="_blank"> <img src="" alt="" id="logo" class="w-100"> </a>
                                <p style="word-wrap: break-word;"><b><small id="logo_link"></small></b></p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <p class="mt-3">Business Permit or PRC ID:</p>
                                <a href="" id="business_permit_link_target" target="_blank"> <img src="" alt="" id="business_permit" class="w-100"></a>
                                <p style="word-wrap: break-word;"><b><small id="business_permit_link"></small></b></p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <p class="mt-3">DTI or SEC Certificate (if applicable):</p>
                                <a href="" id="dti_sec_cert_link_target" target="_blank"> <img src="" alt="" id="dti_sec_cert" class="w-100"></a>
                                <p style="word-wrap: break-word;"><b><small id="dti_sec_cert_link"></small></b></p>
                            </div>
                            <div class="col-sm-12 col-md-3">
                                <p class="mt-3">BIR 2303 Form (Certificate or Registration):</p>
                                <a href="" id="bir_cert_link_target" target="_blank"> <img src="" alt="" id="bir_cert" class="w-100"></a>
                                <p style="word-wrap: break-word;"><b><small id="bir_cert_link"></small></b></p>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection