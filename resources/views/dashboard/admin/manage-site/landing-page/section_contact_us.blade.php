<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Contact Us Section</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-contact-us') }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="scu_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="scu_title" name="title" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->title : old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="scu_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="scu_sub_title" name="sub_title" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->sub_title : old('sub_title') }}">
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="scu_address">Address</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="scu_address" name="address" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->address : old('address') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <label class="col-12" for="scu_contact_number">Contact Number</label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" id="scu_contact_number" name="contact_number" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->contact_number : old('contact_number') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6">
                            <div class="row">
                                <div class="col-12">
                                    <label class="col-12" for="scu_email">Email</label>
                                    <div class="col-12">
                                        <input type="text" class="form-control" id="scu_email" name="email" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->email : old('email') }}">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="scu_facebook_link">Facebook Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="scu_facebook_link" name="facebook_link" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->facebook_link : old('facebook_link') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="scu_twitter_link">Twitter Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="scu_twitter_link" name="twitter_link" placeholder="" value="{{ ($sec_contact_us)? $sec_contact_us->twitter_link : old('twitter_link') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <hr>
        </form>
    </div>
</div>