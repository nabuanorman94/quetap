<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Main Banner</h3>
        <div class="block-options">
            
        </div>
    </div>
    <div class="block-content">
        <form action="{{ route('main-banner-add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($main_banner) ? $main_banner->id : null }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="mbanner_heading_1">Heading 1</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="mbanner_heading_1" name="heading_1" placeholder="" value="{{ ($main_banner)? $main_banner->heading_1 : old('heading_1') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="mbanner_heading_2">Heading 2</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="mbanner_heading_2" name="heading_2" placeholder="" value="{{ ($main_banner)? $main_banner->heading_2 : old('heading_2') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="mbanner_sub_heading">Sub Heading</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="mbanner_sub_heading" name="sub_heading" placeholder="" value="{{ ($main_banner)? $main_banner->sub_heading : old('sub_heading') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="mbanner_video_link">Video Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="mbanner_video_link" name="video_link" placeholder="" value="{{ ($main_banner)? $main_banner->video_link : old('video_link') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        @isset ($main_banner->main_image)
                            <div class="col-4">
                                <div class="col-12">
                                    <img src="{{ $main_banner->main_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="MainBanner" 
                                            data-field="main_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="col-12">
                            <label class="col-12">Main Image <small>580px X 445px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="mbanner_main_image" name="main_image">
                                    <label class="custom-file-label" for="mbanner_main_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        @isset ($main_banner->bg_image)
                            <div class="col-12 col-md-6">
                                <div class="col-12">
                                    <img src="{{ $main_banner->bg_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="MainBanner" 
                                            data-field="bg_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="col-12">
                            <label class="col-12">Background Image <small>1900px X 620px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="mbanner_bg_image" name="bg_image">
                                    <label class="custom-file-label" for="mbanner_bg_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>
    </div>
</div>