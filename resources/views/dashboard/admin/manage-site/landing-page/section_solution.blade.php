<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Solution Section</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-solution') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_solution) ? $sec_solution->id :  null }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="ss_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="ss_title" name="title" placeholder="" value="{{ ($sec_solution)? $sec_solution->title :  old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="ss_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="ss_sub_title" name="sub_title" placeholder="" value="{{ ($sec_solution)? $sec_solution->sub_title :  old('sub_title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="js-ckeditor2">Content</label>
                            <div class="col-12">
                                <textarea class="form-control" id="js-ckeditor2" name="content" placeholder="" rows="10">{!! ($sec_solution)? $sec_solution->content : '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="ss_video">Video Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="ss_video" name="video" placeholder="" value="{{ ($sec_solution)? $sec_solution->video :  old('video') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        @isset ($sec_solution->main_image)
                            <div class="col-4">
                                <div class="col-12">
                                    <img src="{{ $sec_solution->main_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionSolution" 
                                            data-field="main_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="col-12">
                            <label class="col-12">Main Image <small>580px X 445px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="ss_main_image" name="main_image">
                                    <label class="custom-file-label" for="main_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>
    </div>
</div>