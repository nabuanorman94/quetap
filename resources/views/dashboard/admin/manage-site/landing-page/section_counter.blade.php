<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Counter Section(4 columns only)</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-counter') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_counter) ? $sec_counter->id : null }}">
            <div class="row">
                <div class="col-12 col-md-8">
                    <div class="form-group row">
                        @isset ($sec_counter->bg_image)
                            <div class="col-12">
                                <div class="col-12">
                                    <img src="{{ $sec_counter->bg_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionCounter" 
                                            data-field="bg_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12">Background Image <small>1920px X 425px</small></label>
                            <div class="col-12 col-md-8">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="bg_image" name="bg_image">
                                    <label class="custom-file-label" for="bg_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">
                <div class="col-md-3">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Column 1</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_1_title">Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_1_title" name="count_1_title" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_1_title : old('count_1_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_1_icon">Icon <small>Uses Fontawesome 4.7</small></label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_1_icon" name="count_1_icon" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_1_icon : old('count_1_icon') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_1_value">Value</label>
                                <div class="col-12">
                                    <input type="number" class="form-control" id="count_1_value" name="count_1_value" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_1_value : old('count_1_value') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Column 2</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_2_title">Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_2_title" name="count_2_title" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_2_title : old('count_2_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_2_icon">Icon <small>Uses Fontawesome 4.7</small></label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_2_icon" name="count_2_icon" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_2_icon : old('count_2_icon') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_2_value">Value</label>
                                <div class="col-12">
                                    <input type="number" class="form-control" id="count_2_value" name="count_2_value" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_2_value : old('count_2_value') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Column 1</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_3_title">Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_3_title" name="count_3_title" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_3_title : old('count_3_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_3_icon">Icon <small>Uses Fontawesome 4.7</small></label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_3_icon" name="count_3_icon" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_3_icon : old('count_3_icon') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_3_value">Value</label>
                                <div class="col-12">
                                    <input type="number" class="form-control" id="count_3_value" name="count_3_value" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_3_value : old('count_3_value') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Column 1</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_4_title">Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_4_title" name="count_4_title" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_4_title : old('count_4_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_4_icon">Icon <small>Uses Fontawesome 4.7</small></label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="count_4_icon" name="count_4_icon" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_4_icon : old('count_4_icon') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="count_4_value">Value</label>
                                <div class="col-12">
                                    <input type="number" class="form-control" id="count_4_value" name="count_4_value" placeholder="" value="{{ ($sec_counter)? $sec_counter->count_4_value : old('count_4_value') }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>
    </div>
</div>