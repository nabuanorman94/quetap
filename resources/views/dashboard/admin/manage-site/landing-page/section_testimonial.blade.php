<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Testimonial Section</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-testimonial') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_testimonial) ? $sec_testimonial->id : null }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sd_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sd_title" name="title" placeholder="" value="{{ ($sec_testimonial)? $sec_testimonial->title : old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sd_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sd_sub_title" name="sub_title" placeholder="" value="{{ ($sec_testimonial)? $sec_testimonial->sub_title : old('sub_title') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <hr>
        </form>
    </div>
</div>

<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Testimonial Items Section (multiple)</h3>
        <div class="block-options">
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-testimonial-modal"><i class="fa fa-plus"></i> Add</button>
        </div>
    </div>
    <div class="block-content">
        @if (count($testimonial_items) > 0)
            <div class="row">

                @foreach ($testimonial_items as $item)
                    <div class="col-6 col-md-2 text-center testimonial_{{ $item->id }}">
                        <img src="{{ $item->client_image }}" alt="{{ $item->client_name }}" class="w-100 mb-3">
                        <h4>{{ $item->client_name }}</h4>
                        <button type="button" class="btn btn-sm btn-alt-info" onclick="editTestimonial({{$item}})"><i class="fa fa-pencil"></i> Edit</button>
                        <button type="button" class="btn btn-sm btn-alt-secondary" onclick="removeTestimonial({{$item->id}})"><i class="fa fa-trash"></i> Delete</button>
                    </div>
                @endforeach

            </div>
        @else
            <p>No Testimonial Found</p>
        @endif
    </div>
</div>

<div class="modal fade" id="add-testimonial-modal" tabindex="-1" role="dialog" aria-labelledby="add-testimonial-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Add Testimonial</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <form action="{{ route('testimonial-add') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="testimonial_id">
                        <div class="col-12 text-center">
                            <img src="" alt="" class="mb-3" id="client_image_preview" style="display: none;">
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="client_name">Client Name</label>
                                <input type="text" class="form-control" id="client_name" name="client_name" value="{{ old('client_name') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <label for="client_position">Client Position</label>
                                <input type="text" class="form-control" id="client_position" name="client_position" value="{{ old('client_position') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="row">
                                    <label class="col-12">Client Image <small>160px X 160px</small></label>
                                    <div class="col-12">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="client_image" name="client_image">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="row">
                                    <label class="col-12" for="client_said">Content</label>
                                    <div class="col-12">
                                        <textarea class="form-control" id="client_said" name="client_said" placeholder="" rows="10">{!! old('client_said') !!}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group mt-3 text-center">
                            <button type="submit" class="btn btn-alt-primary">
                                <i class="fa fa-floppy-o"></i> Save
                            </button>
                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>