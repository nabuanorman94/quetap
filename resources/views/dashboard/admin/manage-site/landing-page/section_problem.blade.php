<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Problem Section</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-problem') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_problem) ? $sec_problem->id : null }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sp_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sp_title" name="title" placeholder="" value="{{ ($sec_problem)? $sec_problem->title : old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sp_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sp_sub_title" name="sub_title" placeholder="" value="{{ ($sec_problem)? $sec_problem->sub_title : old('sub_title') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sp_video">Video Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sp_video" name="video" placeholder="" value="{{ ($sec_problem)? $sec_problem->video : old('video') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="js-ckeditor1">Content 1</label>
                            <div class="col-12">
                                <textarea class="form-control" id="js-ckeditor1" name="content" placeholder="" rows="10">{!! ($sec_problem)? $sec_problem->content : '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        @isset ($sec_problem->main_image)
                            <div class="col-4">
                                <div class="col-12">
                                    <img src="{{ $sec_problem->main_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionProblem" 
                                            data-field="main_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="col-12">
                            <label class="col-12">Main Image 1 <small>580px X 445px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sp_main_image" name="main_image">
                                    <label class="custom-file-label" for="sp_main_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        @isset ($sec_problem->main_image_2)
                            <div class="col-4">
                                <div class="col-12">
                                    <img src="{{ $sec_problem->main_image_2 }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionProblem" 
                                            data-field="main_image_2"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="col-12">
                            <label class="col-12">Main Image 2 <small>580px X 445px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sp_main_image_2" name="main_image_2">
                                    <label class="custom-file-label" for="sp_main_image_2">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="js-ckeditor4">Content 2</label>
                            <div class="col-12">
                                <textarea class="form-control" id="js-ckeditor4" name="content_2" placeholder="" rows="10">{!! ($sec_problem)? $sec_problem->content_2 : '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="js-ckeditor5">Content 3</label>
                            <div class="col-12">
                                <textarea class="form-control" id="js-ckeditor5" name="content_3" placeholder="" rows="10">{!! ($sec_problem)? $sec_problem->content_3 : '' !!}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        @isset ($sec_problem->main_image_3)
                            <div class="col-4">
                                <div class="col-12">
                                    <img src="{{ $sec_problem->main_image_3 }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionProblem" 
                                            data-field="main_image_3"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                        <div class="col-12">
                            <label class="col-12">Main Image 3 <small>580px X 445px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sp_main_image_3" name="main_image_3">
                                    <label class="custom-file-label" for="sp_main_image_3">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>
    </div>
</div>