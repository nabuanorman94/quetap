<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Download Section</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-download') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_download) ? $sec_download->id : null }}">
            <div class="row">
                <div class="col-12 col-md-7">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sd_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sd_title" name="title" placeholder="" value="{{ ($sec_download)? $sec_download->title : old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sd_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sd_sub_title" name="sub_title" placeholder="" value="{{ ($sec_download)? $sec_download->sub_title : old('sub_title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        @isset ($sec_download->bg_image)
                            <div class="col-12">
                                <div class="col-12">
                                    <img src="{{ $sec_download->bg_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionDownload" 
                                            data-field="bg_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12">Background Image <small>1920px X 425px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sd_bg_image" name="bg_image">
                                    <label class="custom-file-label" for="bg_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <div class="form-group row">
                        @isset ($sec_download->ios_image)
                            <div class="col-6">
                                <div class="col-12">
                                    <img src="{{ $sec_download->ios_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionDownload" 
                                            data-field="ios_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12">App Store Image</label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="ios_image" name="ios_image">
                                    <label class="custom-file-label" for="ios_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="ios_link">App Store Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="ios_link" name="ios_link" placeholder="" value="{{ ($sec_download)? $sec_download->ios_link : old('ios_link') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        @isset ($sec_download->android_image)
                            <div class="col-6">
                                <div class="col-12">
                                    <img src="{{ $sec_download->android_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionDownload" 
                                            data-field="android_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12">Play Store Image</label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="android_image" name="android_image">
                                    <label class="custom-file-label" for="android_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="android_link">Play Store Link</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="android_link" name="android_link" placeholder="" value="{{ ($sec_download)? $sec_download->android_link : old('android_link') }}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>
    </div>
</div>