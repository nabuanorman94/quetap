<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Additional Banners (multiple)</h3>
        <div class="block-options">
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-banner-modal"><i class="fa fa-plus"></i> Add</button>
        </div>
    </div>
    <div class="block-content">
        @if (count($banners) > 0)
            <div id="banner_accordion" role="tablist" aria-multiselectable="true">
            @foreach ($banners as $banner)

                <div class="block block-bordered block-rounded mb-2 banner_{{ $banner->id }}">
                    <div class="block-header" role="tab" id="banner_accordion_h{{ $banner->id }}">
                        <a class="font-w600" data-toggle="collapse" data-parent="#banner_accordion" href="#banner_accordion_q{{ $banner->id }}" aria-expanded="true" aria-controls="banner_accordion_q{{ $banner->id }}">{{ $banner->heading }}</a>
                    </div>
                    <div id="banner_accordion_q{{ $banner->id }}" class="collapse @if ($loop->iteration == 1) {{ 'show' }}  @endif" role="tabpanel" aria-labelledby="banner_accordion_h{{ $banner->id }}" data-parent="#banner_accordion" style="">
                        <div class="block-content p-0">

                            <div class="block">
                                <div class="block-content">
                                    <div class="pull-right">
                                        <button type="button" class="btn btn-sm btn-alt-info" onclick="editBanner({{$banner}})"><i class="fa fa-pencil"></i> Edit</button>
                                        <button type="button" class="btn btn-sm btn-alt-secondary" onclick="removeBanner({{$banner->id}})"><i class="fa fa-trash"></i> Delete</button>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <p class="mb-0">Heading: <b>{{ $banner->heading }}</b></p>
                                            <p class="mb-0">Details: <b>{{ $banner->details }}</b></p>
                                            <p class="mb-0">CTA Link: <b>{{ $banner->cta_link }}</b></p>
                                            <p class="mb-0">CTA Button Label: <b>{{ $banner->cta_button_label }}</b></p>
                                            <p class="mb-0">Orientation: <b> @if ($banner->orientation == 'left') {{ 'Image Left' }} @else {{ 'Image Right' }} @endif</b></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>Main Image</p>
                                            @if ($banner->main_image)
                                                <img src="{{ $banner->main_image }}" alt="{{ $banner->heading }}" class="w-100">
                                                <div class="text-center mt-2 mb-3">
                                                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                        data-model="Banner" 
                                                        data-field="main_image"
                                                        data-myid="{{ $banner->id }}"
                                                    ><i class="fa fa-trash"></i> Remove Image</button>
                                                </div>
                                            @else
                                                <p>No Image Added</p>
                                            @endif
                                            
                                        </div>
                                        <div class="col-md-4">
                                            <p>Background Image</p>
                                            @if ($banner->bg_image)
                                                <img src="{{ $banner->bg_image }}" alt="{{ $banner->bg_image }}" class="w-100">
                                                <div class="text-center mt-2 mb-3">
                                                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                        data-model="Banner" 
                                                        data-field="bg_image"
                                                        data-myid="{{ $banner->id }}"
                                                    ><i class="fa fa-trash"></i> Remove Image</button>
                                                </div>
                                            @else
                                                <p>No Image Added</p>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
    
            @endforeach
            </div>
        @else 
            <p>No Banners Found</p>
        @endif
    </div>
</div>

<div class="modal fade" id="add-banner-modal" tabindex="-1" role="dialog" aria-labelledby="add-banner-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-popout" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Add Banner</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <form action="{{ route('banner-add') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="banner_id">
                        <div class="form-group row">
                            <div class="col-md-5">
                                <label for="banner_heading">Heading</label>
                                <input type="text" class="form-control" id="banner_heading" name="banner_heading" value="{{ old('banner_heading') }}">
                            </div>
                            <div class="col-md-7">
                                <label for="banner_details">Details</label>
                                <input type="text" class="form-control" id="banner_details" name="banner_details" value="{{ old('banner_details') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-7">
                                <label for="cta_link">CTA Link <small> within the site</small></label>
                                <input type="text" class="form-control" id="cta_link" name="cta_link" value="{{ old('cta_link') }}">
                            </div>
                            <div class="col-md-5">
                                <label for="cta_button_label">CTA Button Label</label>
                                <input type="text" class="form-control" id="cta_button_label" name="cta_button_label" value="{{ old('cta_button_label') }}">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="orientation">Orientation <small> banner layout</small></label>
                            <select class="form-control" id="orientation" name="orientation">
                                <option value="left" @if (old('orientation') == 'left') {{ 'selected' }} @endif>Image Left</option>
                                <option value="right" @if (old('orientation') == 'right') {{ 'selected' }} @endif>Image Right</option>
                            </select>
                        </div>

                        <div class="form-group row">

                            {{-- @if ()
                                <div class="col">
                                    <img src="" alt="">
                                </div>
                            @endif --}}

                            <div class="col-md-5">
                                <div class="row">
                                    <label class="col-12">Main Image <small>580px X 445px</small></label>
                                    <div class="col-12">
                                        <img src="" alt="" class="w-75 mb-3" id="banner_main_image_preview">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="banner_main_image" name="banner_main_image">
                                            <label class="custom-file-label" for="banner_main_image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="row">
                                    <label class="col-12">Background Image <small>1900px X 620px</small></label>
                                    <div class="col-12">
                                        <img src="" alt="" class="w-75 mb-3" id="banner_bg_image_preview">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="banner_bg_image" name="banner_bg_image">
                                            <label class="custom-file-label" for="banner_bg_image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="form-group mt-3 text-center">
                            <button type="submit" class="btn btn-alt-primary">
                                <i class="fa fa-floppy-o"></i> Save
                            </button>
                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>

                    </form>
                </div>
            </div>
            {{-- <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-alt-success" data-dismiss="modal">
                    <i class="fa fa-check"></i> Perfect
                </button>
            </div> --}}
        </div>
    </div>
</div>