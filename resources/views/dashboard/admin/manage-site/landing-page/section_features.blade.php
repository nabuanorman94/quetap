<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Features Section (6 featured items only)</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-features') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_feature) ? $sec_feature->id : null }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sf_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sf_title" name="title" placeholder="" value="{{ ($sec_feature)? $sec_feature->title : old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sf_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sf_sub_title" name="sub_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->sub_title : old('sub_title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12">Main Image <small>275px X 540px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sf_main_image" name="main_image">
                                    <label class="custom-file-label" for="main_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        @isset ($sec_feature->main_image)
                            <div class="col-12 col-md-4">
                                <div class="col-12">
                                    <img src="{{ $sec_feature->main_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionFeature" 
                                            data-field="main_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>

            <hr>

            <div class="row">

                <div class="col-md-4">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Feature 1</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_1_title">Feature Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="feat_1_title" name="feat_1_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_1_title : old('feat_1_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_1_content">Feature Content</label>
                                <div class="col-12">
                                    {{-- <input type="text" class="form-control" id="feat_1_content" name="feat_1_content" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_1_content : old('feat_1_content') }}"> --}}
                                    <textarea class="form-control" id="feat_1_content" name="feat_1_content" rows="3">{{ ($sec_feature)? $sec_feature->feat_1_content : old('feat_1_content') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            @isset ($sec_feature->feat_1_icon)
                                <div class="col-4">
                                    <div class="col-12">
                                        <img src="{{ $sec_feature->feat_1_icon }}" class="w-100 mb-2" alt="">
                                        <div class="text-center mt-2 mb-3">
                                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                data-model="SectionFeature" 
                                                data-field="feat_1_icon"
                                            ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-12">
                                <label class="col-12">Feature Icon <small>59px X 69px</small></label>
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="feat_1_icon" name="feat_1_icon">
                                        <label class="custom-file-label" for="feat_1_icon">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Feature 2</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_2_title">Feature Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="feat_2_title" name="feat_2_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_2_title : old('feat_2_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_2_content">Feature Content</label>
                                <div class="col-12">
                                    {{-- <input type="text" class="form-control" id="feat_2_content" name="feat_2_content" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_2_content : old('feat_2_content') }}"> --}}
                                    <textarea class="form-control" id="feat_2_content" name="feat_2_content" rows="3">{{ ($sec_feature)? $sec_feature->feat_2_content : old('feat_2_content') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            @isset ($sec_feature->feat_2_icon)
                                <div class="col-4">
                                    <div class="col-12">
                                        <img src="{{ $sec_feature->feat_2_icon }}" class="w-100 mb-2" alt="">
                                        <div class="text-center mt-2 mb-3">
                                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                data-model="SectionFeature" 
                                                data-field="feat_2_icon"
                                            ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-12">
                                <label class="col-12">Feature Icon <small>59px X 69px</small></label>
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="feat_2_icon" name="feat_2_icon">
                                        <label class="custom-file-label" for="feat_2_icon">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Feature 3</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_1_title">Feature Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="feat_3_title" name="feat_3_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_3_title : old('feat_3_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_3_content">Feature Content</label>
                                <div class="col-12">
                                    {{-- <input type="text" class="form-control" id="feat_3_content" name="feat_3_content" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_3_content : old('feat_3_content') }}"> --}}
                                    <textarea class="form-control" id="feat_3_content" name="feat_3_content" rows="3">{{ ($sec_feature)? $sec_feature->feat_3_content : old('feat_3_content') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            @isset ($sec_feature->feat_3_icon)
                                <div class="col-4">
                                    <div class="col-12">
                                        <img src="{{ $sec_feature->feat_3_icon }}" class="w-100 mb-2" alt="">
                                        <div class="text-center mt-2 mb-3">
                                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                data-model="SectionFeature" 
                                                data-field="feat_3_icon"
                                            ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-12">
                                <label class="col-12">Feature Icon <small>59px X 69px</small></label>
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="feat_3_icon" name="feat_3_icon">
                                        <label class="custom-file-label" for="feat_3_icon">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Feature 4</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_4_title">Feature Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="feat_4_title" name="feat_4_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_4_title : old('feat_4_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_4_content">Feature Content</label>
                                <div class="col-12">
                                    {{-- <input type="text" class="form-control" id="feat_4_content" name="feat_4_content" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_4_content : old('feat_4_content') }}"> --}}
                                    <textarea class="form-control" id="feat_4_content" name="feat_4_content" rows="3">{{ ($sec_feature)? $sec_feature->feat_4_content : old('feat_4_content') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            @isset ($sec_feature->feat_4_icon)
                                <div class="col-4">
                                    <div class="col-12">
                                        <img src="{{ $sec_feature->feat_4_icon }}" class="w-100 mb-2" alt="">
                                        <div class="text-center mt-2 mb-3">
                                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                data-model="SectionFeature" 
                                                data-field="feat_4_icon"
                                            ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-12">
                                <label class="col-12">Feature Icon <small>59px X 69px</small></label>
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="feat_4_icon" name="feat_4_icon">
                                        <label class="custom-file-label" for="feat_4_icon">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Feature 5</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_5_title">Feature Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="feat_5_title" name="feat_5_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_5_title : old('feat_5_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_5_content">Feature Content</label>
                                <div class="col-12">
                                    {{-- <input type="text" class="form-control" id="feat_5_content" name="feat_5_content" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_5_content : old('feat_5_content') }}"> --}}
                                    <textarea class="form-control" id="feat_5_content" name="feat_5_content" rows="3">{{ ($sec_feature)? $sec_feature->feat_5_content : old('feat_5_content') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            @isset ($sec_feature->feat_5_icon)
                                <div class="col-4">
                                    <div class="col-12">
                                        <img src="{{ $sec_feature->feat_5_icon }}" class="w-100 mb-2" alt="">
                                        <div class="text-center mt-2 mb-3">
                                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                data-model="SectionFeature" 
                                                data-field="feat_5_icon"
                                            ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-12">
                                <label class="col-12">Feature Icon <small>59px X 69px</small></label>
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="feat_5_icon" name="feat_5_icon">
                                        <label class="custom-file-label" for="feat_5_icon">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="block block-rounded p-2 bg-gray-lighter">
                        <div class="block-header">
                            <h3 class="block-title font-w700 text-center">Feature 6</h3>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_6_title">Feature Title</label>
                                <div class="col-12">
                                    <input type="text" class="form-control" id="feat_6_title" name="feat_6_title" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_6_title : old('feat_6_title') }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-12">
                                <label class="col-12" for="feat_6_content">Feature Content</label>
                                <div class="col-12">
                                    {{-- <input type="text" class="form-control" id="feat_6_content" name="feat_6_content" placeholder="" value="{{ ($sec_feature)? $sec_feature->feat_6_content : old('feat_6_content') }}"> --}}
                                    <textarea class="form-control" id="feat_6_content" name="feat_6_content" rows="3">{{ ($sec_feature)? $sec_feature->feat_6_content : old('feat_6_content') }}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            @isset ($sec_feature->feat_6_icon)
                                <div class="col-4">
                                    <div class="col-12">
                                        <img src="{{ $sec_feature->feat_6_icon }}" class="w-100 mb-2" alt="">
                                        <div class="text-center mt-2 mb-3">
                                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                                data-model="SectionFeature" 
                                                data-field="feat_6_icon"
                                            ><i class="fa fa-trash"></i></button>
                                        </div>
                                    </div>
                                </div>
                            @endisset
                            <div class="col-12">
                                <label class="col-12">Feature Icon <small>59px X 69px</small></label>
                                <div class="col-12">
                                    <div class="custom-file">
                                        <input type="file" class="custom-file-input" id="feat_6_icon" name="feat_6_icon">
                                        <label class="custom-file-label" for="feat_6_icon">Choose file</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
        </form>
    </div>
</div>