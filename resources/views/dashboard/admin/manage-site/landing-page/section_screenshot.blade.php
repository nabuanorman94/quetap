<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Screenshot Section | How It Works</h3>
    </div>
    <div class="block-content">
        <form action="{{ route('section-screenshot') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{ ($sec_screenshot) ? $sec_screenshot->id : null }}">
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sss_title">Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sss_title" name="title" placeholder="" value="{{ ($sec_screenshot)? $sec_screenshot->title : old('title') }}">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12" for="sss_sub_title">Sub Title</label>
                            <div class="col-12">
                                <input type="text" class="form-control" id="sss_sub_title" name="sub_title" placeholder="" value="{{ ($sec_screenshot)? $sec_screenshot->sub_title : old('sub_title') }}">
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12">
                            <label class="col-12">Main Image <small>275px X 540px</small></label>
                            <div class="col-12">
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="sss_main_image" name="main_image">
                                    <label class="custom-file-label" for="main_image">Choose file</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <div class="form-group row">
                        @isset ($sec_screenshot->main_image)
                            <div class="col-4">
                                <div class="col-12">
                                    <img src="{{ $sec_screenshot->main_image }}" class="w-100 mb-2" alt="">
                                    <div class="text-center mt-2 mb-3">
                                        <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                            data-model="SectionScreenshot" 
                                            data-field="main_image"
                                        ><i class="fa fa-trash"></i> Remove Image</button>
                                    </div>
                                </div>
                            </div>
                        @endisset
                    </div>
                </div>
            </div>
            <div class="mt-4 mb-3 text-center">
                <button type="submit" class="btn btn-alt-primary"><i class="fa fa-floppy-o"></i> Save</button>
                <button type="button" class="btn btn-secondary" onclick="cancelEdit()"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <hr>
        </form>
    </div>
</div>

<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Screenshot Images Section (multiple) <small>Recommended at least 6 items</small></h3>
        <div class="block-options">
            <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add-screenshot-modal"><i class="fa fa-plus"></i> Add</button>
        </div>
    </div>
    <div class="block-content">
        @if (count($screenshot_images) > 0)
            <div class="row">

                @foreach ($screenshot_images as $screenshot_image)
                    <div class="col-6 col-md-2 text-center">
                        <img src="{{ $screenshot_image->image }}" alt="{{ $screenshot_image->image_alt }}" class="w-100 mb-3">
                        {{-- <button type="button" class="btn btn-sm btn-alt-info" onclick="editScreenshot({{$screenshot_image}})"><i class="fa fa-pencil"></i> Edit</button> --}}
                        <div class="text-center mt-2 mb-3">
                            <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                data-model="SectionScreenshotImage" 
                                data-myid="{{ $screenshot_image->id }}"
                            ><i class="fa fa-trash"></i> Remove Image</button>
                        </div>
                    </div>
                @endforeach

            </div>
        @else
            <p>No Screenshot Found</p>
        @endif
    </div>
</div>

<div class="modal fade" id="add-screenshot-modal" tabindex="-1" role="dialog" aria-labelledby="add-screenshot-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popout" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent">
                <div class="block-header bg-primary">
                    <h3 class="block-title">Add Screenshot</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <form action="{{ route('screenshot-add') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="screenshot_id">
                        <div class="form-group row">
                            <div class="col-12">
                                <label for="image_alt">Description</label>
                                <input type="text" class="form-control" id="image_alt" name="image_alt" value="{{ old('image_alt') }}">
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-12">
                                <div class="row">
                                    <label class="col-12">Image <small>213px X 379px</small></label>
                                    <div class="col-12">
                                        <img src="" alt="" class="w-50 mb-3" id="screenshot_image_preview" style="display: none;">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="image" name="image">
                                            <label class="custom-file-label" for="image">Choose file</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <div class="form-group mt-3 text-center">
                            <button type="submit" class="btn btn-alt-primary">
                                <i class="fa fa-floppy-o"></i> Save
                            </button>
                            <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">
                                <i class="fa fa-times"></i> Cancel
                            </button>
                        </div>

                    </form>
                </div>
            </div>

        </div>
    </div>
</div>