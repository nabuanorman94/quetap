@extends('layouts.backend')

@section('title')
    Manage Site | 
@endsection

@inject('admin', 'App\Http\Controllers\AdminController')

@section('css_after')
<link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
<script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script>
<!-- Page JS Helpers (CKEditor plugin) -->
<script>jQuery(function(){ Codebase.helpers(['ckeditor']); });</script>
<script>
    $(document).ready(function(){
        $('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
            localStorage.setItem('activeTab', $(e.target).attr('href'));
        });
        var activeTab = localStorage.getItem('activeTab');
        if(activeTab){
            $('#siteManagementTab a[href="' + activeTab + '"]').tab('show');
        }

        // TO DO
        // Make image display on edit, make swal and ajax on image remove, position banner based on orientation

        setTimeout(() => {
            $(".alert").fadeOut(1000);
        }, 4000)

        // Clear all form modals
        clearBannerForm();
        clearSreenshotForm();
        clearTestimonialForm();
    });

    $(".custom-file-input").on("change", function() {
        var fileName = $(this).val().split("\\").pop();
        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
    });

    $("#add-banner-modal").on("hidden.bs.modal", function () {
        clearBannerForm();
    });

    function editBanner(banner) {
        $('input[name="banner_id"]').val(banner.id);
        $('input[name="banner_heading"]').val(banner.heading);
        $('input[name="banner_details"]').val(banner.details);
        $('input[name="cta_link"]').val(banner.cta_link);
        $('input[name="cta_button_label"]').val(banner.cta_button_label);
        $('select[name="orientation"]').val(banner.orientation);

        if(banner.main_image) {
            $('#banner_main_image_preview').show().attr('src', banner.main_image);
        } else {
            $('#banner_main_image_preview').hide();
        }

        $('#banner_bg_image_preview').show().attr('src', banner.bg_image);

        $('#add-banner-modal').modal('show');
    }

    function removeBanner(bannerid) {
        swal.fire({
            title: "Are you sure?",
            text: "You will not be able to recover action!",
            icon: "warning",
            showCancelButton: !0,
            buttonsStyling: !1,
            customClass: {
                confirmButton: "btn btn-alt-success m-5",
                cancelButton: "btn btn-alt-danger m-5",
                input: "form-control"
            },
            confirmButtonText: "Yes, delete it!",
            html: !1,
        })
        .then((result) => {
            if(result.value) {
                $.ajax({
                    url: "{{ route('banner-remove') }}",
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        myid: bannerid
                    }
                    // context: document.body
                }).done(function() {
                    $('.banner_'+bannerid).remove()
                });
            }
        })
    }

    function clearBannerForm() {
        $('input[name="banner_id"]').val('');
        $('input[name="banner_heading"]').val('');
        $('input[name="banner_details"]').val('');
        $('input[name="cta_link"]').val('');
        $('input[name="cta_button_label"]').val('');
        $('select[name="orientation"]').val();
        $('#banner_main_image_preview').hide();
        $('#banner_bg_image_preview').hide();
    }

    $("#add-screenshot-modal").on("hidden.bs.modal", function () {
        clearSreenshotForm();
    });

    function editScreenshot(screenshot) {
        $('input[name="screenshot_id"]').val(screenshot.id);
        $('input[name="image_alt"]').val(screenshot.image_alt);
        $('#screenshot_image_preview').show().attr('src', screenshot.image);
        $('#add-screenshot-modal').modal('show');
    }

    function clearSreenshotForm() {
        $('input[name="screenshot_id"]').val('');
        $('input[name="image_alt"]').val('');
        $('#screenshot_image_preview').hide();
    }

    $("#add-testimonial-modal").on("hidden.bs.modal", function () {
        clearTestimonialForm();
    });

    function editTestimonial(testimonial) {
        $('input[name="testimonial_id"]').val(testimonial.id);
        $('input[name="client_name"]').val(testimonial.client_name);
        $('input[name="client_position"]').val(testimonial.client_position);
        $('#client_said').html(testimonial.client_said);
        $('#client_image_preview').show().attr('src', testimonial.client_image);
        $('#add-testimonial-modal').modal('show');
    }

    function removeTestimonial(testimonialid) {
        swal.fire({
            title: "Are you sure?",
            text: "You will not be able to recover action!",
            icon: "warning",
            showCancelButton: !0,
            buttonsStyling: !1,
            customClass: {
                confirmButton: "btn btn-alt-success m-5",
                cancelButton: "btn btn-alt-danger m-5",
                input: "form-control"
            },
            confirmButtonText: "Yes, delete it!",
            html: !1,
        })
        .then((result) => {
            if(result.value) {
                $.ajax({
                    url: "{{ route('testimonial-remove') }}",
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        myid: testimonialid
                    }
                    // context: document.body
                }).done(function() {
                    $('.testimonial_'+testimonialid).remove()
                });
            }
        })
    }

    function clearTestimonialForm() {
        $('input[name="testimonial_id"]').val('');
        $('input[name="client_name"]').val('');
        $('input[name="client_position"]').val('');
        $('#client_said').html('');
        $('#client_image_preview').hide();
    }

    function cancelEdit() {
        window.location.reload(true);
    }

    function removeImageFor(data) {
        swal.fire({
            title: "Are you sure?",
            text: "You will not be able to recover this image!",
            icon: "warning",
            showCancelButton: !0,
            buttonsStyling: !1,
            customClass: {
                confirmButton: "btn btn-alt-success m-5",
                cancelButton: "btn btn-alt-danger m-5",
                input: "form-control"
            },
            confirmButtonText: "Yes, delete it!",
            html: !1,
        })
        .then((result) => {
            if(result.value) {
                $.ajax({
                    url: "{{ route('remove-image') }}",
                    method: "POST",
                    data: {
                        _token: "{{ csrf_token() }}",
                        model: ($(data).data('model'))? $(data).data('model') : null,
                        field: ($(data).data('field'))? $(data).data('field') : null,
                        myid: ($(data).data('myid'))? $(data).data('myid') : null,
                    }
                    // context: document.body
                }).done(function() {
                    swal.fire({
                        title: "Success!",
                        text: "Please reload your page.",
                        icon: "success",
                        buttonsStyling: !1,
                        customClass: {
                            confirmButton: "btn btn-alt-success m-5",
                            input: "form-control"
                        },
                        confirmButtonText: "Reload now",
                        allowOutsideClick: false,
                        html: !1,
                    })
                    .then((result) => {
                        if(result.value) {
                            window.location.reload(true)
                        }
                    })
                });
            }
        })
    }
</script>
@endsection

@section('content')
    <div id="app" class="content">
        <h2 class="content-heading"><i class="fa fa-sitemap"></i> Manage Site <small>Landing Page</small></h2>

        {{-- <nav class="breadcrumb bg-white push">
            <a class="breadcrumb-item" href="javascript:void(0)">Generic</a>
            <span class="breadcrumb-item active">Blank Page</span>
        </nav> --}}

        @include('common.simple-alerts')

        <div class="block">
            <ul class="nav nav-tabs nav-tabs-alt" id="siteManagementTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="main-banner-tab" data-toggle="tab" href="#main-banner" role="tab" aria-controls="main-banner" aria-selected="true">Main Banner</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="additional-banners-tab" data-toggle="tab" href="#additional-banners" role="tab" aria-controls="additional-banners" aria-selected="false">Additional Banners</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="problem-section-tab" data-toggle="tab" href="#problem-section" role="tab" aria-controls="problem-section" aria-selected="false">Problem</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="solution-section-tab" data-toggle="tab" href="#solution-section" role="tab" aria-controls="solution-section" aria-selected="false">Solution</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="features-section-tab" data-toggle="tab" href="#features-section" role="tab" aria-controls="features-section" aria-selected="false">Features</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="counter-section-tab" data-toggle="tab" href="#counter-section" role="tab" aria-controls="counter-section" aria-selected="false"> Counter</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="screenshot-section-tab" data-toggle="tab" href="#screenshot-section" role="tab" aria-controls="screenshot-section" aria-selected="false"> Screenshots</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="download-section-tab" data-toggle="tab" href="#download-section" role="tab" aria-controls="download-section" aria-selected="false"> Download</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="testimonial-section-tab" data-toggle="tab" href="#testimonial-section" role="tab" aria-controls="testimonial-section" aria-selected="false"> Testimonials</a>
                </li>
                <li class="nav-item" role="presentation">
                    <a class="nav-link" id="contact-section-tab" data-toggle="tab" href="#contact-section" role="tab" aria-controls="contact-section" aria-selected="false"> Contact Us</a>
                </li>
            </ul>
            <div class="block-content tab-content" id="siteManagementTabContent">
                <div class="tab-pane fade show active" id="main-banner" role="tabpanel" aria-labelledby="main-banner-tab">
                    @include('dashboard.admin.manage-site.landing-page.banner_main')
                </div>
                <div class="tab-pane fade" id="additional-banners" role="tabpanel" aria-labelledby="additional-banners-tab">
                    @include('dashboard.admin.manage-site.landing-page.banners')
                </div>
                <div class="tab-pane fade" id="problem-section" role="tabpanel" aria-labelledby="problem-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_problem')
                </div>
                <div class="tab-pane fade" id="solution-section" role="tabpanel" aria-labelledby="solution-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_solution')
                </div>
                <div class="tab-pane fade" id="features-section" role="tabpanel" aria-labelledby="features-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_features')
                </div>
                <div class="tab-pane fade" id="counter-section" role="tabpanel" aria-labelledby="counter-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_counter')
                </div>
                <div class="tab-pane fade" id="screenshot-section" role="tabpanel" aria-labelledby="screenshot-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_screenshot')
                </div>
                <div class="tab-pane fade" id="download-section" role="tabpanel" aria-labelledby="download-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_download')
                </div>
                <div class="tab-pane fade" id="testimonial-section" role="tabpanel" aria-labelledby="testimonial-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_testimonial')
                </div>
                <div class="tab-pane fade" id="contact-section" role="tabpanel" aria-labelledby="contact-section-tab">
                    @include('dashboard.admin.manage-site.landing-page.section_contact_us')
                </div>
            </div>
        </div>

    </div>
@endsection
