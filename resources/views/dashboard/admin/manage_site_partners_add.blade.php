@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script>
    <!-- Page JS Helpers (CKEditor plugin) -->
    <script>jQuery(function(){ Codebase.helpers(['ckeditor']); });</script>

    <script>
        $(document).ready(function(){

            $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });

            setTimeout(() => {
                $(".alert").fadeOut(1000);
            }, 4000);

        });

        function removeImageFor(data) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover this image!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-image') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            model: ($(data).data('model'))? $(data).data('model') : null,
                            field: ($(data).data('field'))? $(data).data('field') : null,
                            myid: ($(data).data('myid'))? $(data).data('myid') : null,
                        }
                        // context: document.body
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Please reload your page.",
                            icon: "success",
                            buttonsStyling: !1,
                            customClass: {
                                confirmButton: "btn btn-alt-success m-5",
                                input: "form-control"
                            },
                            confirmButtonText: "Reload now",
                            allowOutsideClick: false,
                            html: !1,
                        })
                        .then((result) => {
                            if(result.value) {
                                window.location.reload(true)
                            }
                        })
                    });
                }
            })
        }

    </script>
@endsection

@section('title')
    {{ (isset($details))? 'Edit Details' : 'Add Details' }} | Manage Site | 
@endsection

@section('content')
    <div id="app" class="content">
        <h2 class="content-heading"><i class="fa fa-sitemap"></i> Manage Site <small>Partners Page</small> > <small>{{ (isset($details))? 'Edit Details' : 'Add Details' }}</small></h2>

        @include('common.simple-alerts')

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{ (isset($details))? 'Edit Page' : 'New Page' }}</h3>
            </div>
            <div class="block-content">
                <form action="{{ route('add-partners-page-details') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="details_id" value="{{ (isset($details))? $details->id : null }}">

                    <div class="form-group row">
                        <div class="col-sm-12 col-md-8">
                            <label for="js-ckeditor9">Content</label>
                            <textarea class="form-control" id="js-ckeditor9" name="content" placeholder="" rows="10">{!! (isset($details))?  $details->content : old('content') !!}</textarea>
                        </div>

                        <div class="col-sm-12 col-md-4">
                            <label>Image <small>580px X 445px</small></label>
                            
                            @isset($details)
                                @if (isset($details->image))
                                <img src="{{ $details->image }}" alt="" class="w-100 mb-3" id="image_preview">
                                <div class="text-center mb-3">
                                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                        data-model="PartnerPageDetails" 
                                        data-field="image"
                                        data-myid="{{ $details->id }}"
                                    ><i class="fa fa-trash"></i> Remove Image</button>
                                </div>
                                @endif
                            @endisset

                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="image" name="image">
                                <label class="custom-file-label" for="image">Choose file</label>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="form-group mt-3 text-center">
                        <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-floppy-o"></i> Save
                        </button>
                        <a href="{{ route('manage-site-partners') }}" class="btn btn-alt-secondary">
                            <i class="fa fa-times"></i> Cancel
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection