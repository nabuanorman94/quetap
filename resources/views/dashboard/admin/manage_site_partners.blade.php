@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/plugins/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- Page JS Code -->
    <script src="{{ asset('js/pages/tables_datatables.js') }}"></script>

    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>

    <script>
        $(document).ready(function(){
            setTimeout(() => {
                $(".alert").fadeOut(1000);
            }, 4000);
        });

        function removePage(pageId) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover action!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove--partners-page-details') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            myid: pageId
                        }
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Please reload your page.",
                            icon: "success",
                            buttonsStyling: !1,
                            customClass: {
                                confirmButton: "btn btn-alt-success m-5",
                                input: "form-control"
                            },
                            confirmButtonText: "Reload now",
                            allowOutsideClick: false,
                            html: !1,
                        })
                        .then((result) => {
                            if(result.value) {
                                window.location.reload(true)
                            }
                        })
                    });
                }
            })
        }
    </script>
@endsection

@section('title')
    Partners Page | Manage Site | 
@endsection

@section('content')
    <div id="app" class="content">
        <h2 class="content-heading"><i class="fa fa-sitemap"></i> Manage Site <small>Partners Page</small></h2>

        @include('common.simple-alerts')

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Details</h3>
                <div class="block-options">
                    {{-- <button type="button" class="btn-block-option">
                        <i class="fa fa-plus"></i> Add Page
                    </button> --}}
                    <a href="{{ route('add-partners-page-details-view') }}" type="button" class="btn btn-sm btn-success">
                        <i class="fa fa-plus"></i> Add Details
                    </a>
                </div>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 80px;">ID</th>
                            <th>Content</th>
                            <th>Image</th>
                            <th class="text-center" style="width: 15%;">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($partners_page_details as $details)
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td>{!! $details->content !!}</td>
                                <td class="text-center"><img src="{{ $details->image }}" alt="{{$details->heading}}" style="max-width: 200px;"></td>
                                <td class="text-center">
                                    <a href="{{ route('edit-partners-page-details', $details->id) }}" class="btn btn-sm btn-alt-info"><i class="fa fa-pencil"></i> Edit</a>
                                    <button class="btn btn-sm btn-alt-secondary" onclick="removePage({{ $details->id }})"><i class="fa fa-trash"></i> Delete</button>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection