@extends('layouts.backend')

@section('css_before')
    <!-- Page JS Plugins CSS -->
    <link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <!-- Page JS Plugins -->
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('js/plugins/ckeditor/ckeditor.js') }}"></script>
    <!-- Page JS Helpers (CKEditor plugin) -->
    <script>jQuery(function(){ Codebase.helpers(['ckeditor']); });</script>

    <script>
        $(document).ready(function(){

            $(".custom-file-input").on("change", function() {
                var fileName = $(this).val().split("\\").pop();
                $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
            });

        });

        function removeImageFor(data) {
            swal.fire({
                title: "Are you sure?",
                text: "You will not be able to recover this image!",
                icon: "warning",
                showCancelButton: !0,
                buttonsStyling: !1,
                customClass: {
                    confirmButton: "btn btn-alt-success m-5",
                    cancelButton: "btn btn-alt-danger m-5",
                    input: "form-control"
                },
                confirmButtonText: "Yes, delete it!",
                html: !1,
            })
            .then((result) => {
                if(result.value) {
                    $.ajax({
                        url: "{{ route('remove-image') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}",
                            model: ($(data).data('model'))? $(data).data('model') : null,
                            field: ($(data).data('field'))? $(data).data('field') : null,
                            myid: ($(data).data('myid'))? $(data).data('myid') : null,
                        }
                        // context: document.body
                    }).done(function() {
                        swal.fire({
                            title: "Success!",
                            text: "Please reload your page.",
                            icon: "success",
                            buttonsStyling: !1,
                            customClass: {
                                confirmButton: "btn btn-alt-success m-5",
                                input: "form-control"
                            },
                            confirmButtonText: "Reload now",
                            allowOutsideClick: false,
                            html: !1,
                        })
                        .then((result) => {
                            if(result.value) {
                                window.location.reload(true)
                            }
                        })
                    });
                }
            })
        }

    </script>
@endsection

@section('title')
    {{ (isset($page))? 'Edit Page' : 'Add Page' }} | Manage Site | 
@endsection

@section('content')
    <div id="app" class="content">
        <h2 class="content-heading"><i class="fa fa-sitemap"></i> Manage Site <small>Other Pages</small> > <small>{{ (isset($page))? 'Edit Page' : 'Add Page' }}</small></h2>

        @include('common.simple-alerts')

        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">{{ (isset($page))? 'Edit Page' : 'New Page' }}</h3>
            </div>
            <div class="block-content">
                <form action="{{ route('add-page') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="page_id" value="{{ (isset($page))? $page->id : null }}">
                    <div class="form-group row">
                        <div class="col-md-6">
                            <label for="title">Title</label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ (isset($page))?  $page->title : old('title') }}">
                        </div>
                        <div class="col-md-6">
                            <label for="slug">Slug</label>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{ (isset($page))?  $page->slug : old('slug') }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label>Background Image <small>1920px X 425px</small></label>
                            
                            @isset($page)
                                @if (isset($page->bg_image))
                                <img src="{{ $page->bg_image }}" alt="" class="w-100 mb-3" id="bg_image_preview">
                                <div class="text-center mb-3">
                                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                        data-model="Page" 
                                        data-field="bg_image"
                                        data-myid="{{ $page->id }}"
                                    ><i class="fa fa-trash"></i> Remove Image</button>
                                </div>
                                @endif
                            @endisset

                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="bg_image" name="bg_image">
                                <label class="custom-file-label" for="bg_image">Choose file</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="meta_tags">Meta Tags</label>
                            <textarea class="form-control" id="meta_tags" name="meta_tags" placeholder="" rows="5">{!! (isset($page))?  $page->meta_tags : old('meta_tags') !!}</textarea>
                        </div>
                    </div>

                    <hr>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="js-ckeditor6">Main Content</label>
                            <textarea class="form-control" id="js-ckeditor6" name="main_content" placeholder="" rows="10">{!! (isset($page))?  $page->main_content : old('main_content') !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="js-ckeditor7">Content 1</label>
                            <textarea class="form-control" id="js-ckeditor7" name="content" placeholder="" rows="10">{!! (isset($page))?  $page->content : old('content') !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12 col-md-6 text-center">
                            @isset($page)
                                @if (isset($page->main_image))
                                <img src="{{ $page->main_image }}" alt="" class="w-50 mb-3" id="main_image_preview">
                                <div class="text-center mb-3">
                                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                        data-model="Page" 
                                        data-field="main_image"
                                        data-myid="{{ $page->id }}"
                                    ><i class="fa fa-trash"></i> Remove Image</button>
                                </div>
                                @endif
                            @endisset
                        </div>
                        <div class="col-md-12">
                            <label>Image 1 <small>580px X 445px</small></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="main_image" name="main_image">
                                <label class="custom-file-label" for="main_image">Choose file</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <label for="js-ckeditor8">Content 2</label>
                            <textarea class="form-control" id="js-ckeditor8" name="content_2" placeholder="" rows="10">{!! (isset($page))?  $page->content_2 : old('content_2') !!}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-12 col-md-6 text-center">
                            @isset($page)
                                @if (isset($page->main_image_2))
                                <img src="{{ $page->main_image_2 }}" alt="" class="w-50 mb-3" id="main_image_2_preview">
                                <div class="text-center mb-3">
                                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="removeImageFor(this)" 
                                        data-model="Page" 
                                        data-field="main_image_2"
                                        data-myid="{{ $page->id }}"
                                    ><i class="fa fa-trash"></i> Remove Image</button>
                                </div>
                                @endif
                            @endisset
                        </div>
                        <div class="col-md-12">
                            <label>Image 2 <small>580px X 445px</small></label>
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="main_image_2" name="main_image_2">
                                <label class="custom-file-label" for="main_image_2">Choose file</label>
                            </div>
                        </div>
                    </div>

                    <hr>
                    <div class="form-group mt-3 text-center">
                        <button type="submit" class="btn btn-alt-primary">
                            <i class="fa fa-floppy-o"></i> Save
                        </button>
                        <a href="{{ route('manage-site-other-pages') }}" class="btn btn-alt-secondary">
                            <i class="fa fa-times"></i> Cancel
                        </button>
                    </div>

                </form>
            </div>
        </div>
    </div>
@endsection