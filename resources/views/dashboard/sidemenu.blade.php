<ul class="nav-main">
    @if(Auth::user()->is_admin)
        <li>
            <a class="{{ request()->is('dashboard') ? ' active' : '' }}" href="{{ route('admin') }}">
                <i class="si si-cup"></i>
                <span class="sidebar-mini-hide">Dashboard</span>
            </a>
        </li>
        {{-- <li>
            <a class="{{ request()->is('dashboard/manage-site') ? ' active' : '' }}" href="{{ route('manage-site-landing-page') }}" onclick="removeActiveTab()">
                <i class="fa fa-sitemap"></i>
                <span class="sidebar-mini-hide">Manage Site</span>
            </a>
        </li> --}}
        <li class="{{ request()->is('dashboard/manage-site/*') ? ' open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                <i class="fa fa-sitemap"></i>
                <span class="sidebar-mini-hide">Manage Site</span>
            </a>
            <ul>
                <li>
                    <a class="{{ request()->is('dashboard/manage-site/landing-page') ? ' active' : '' }}" href="{{ route('manage-site-landing-page') }}" onclick="removeActiveTab()">Landing Page</a>
                </li>
                <li>
                    <a class="{{ request()->is('dashboard/manage-site/other-pages') || request()->is('dashboard/manage-site/add-page') || request()->is('dashboard/manage-site/edit-page/*') ? ' active' : '' }}" href="{{ route('manage-site-other-pages') }}">Other Pages</a>
                </li>
                <li>
                    <a class="{{ request()->is('dashboard/manage-site/messages') ? ' active' : '' }}" href="{{ route('manage-site-messages') }}">Messages</a>
                </li>
                <li>
                    <a class="{{ request()->is('dashboard/manage-site/partners-page') ? ' active' : '' }}" href="{{ route('manage-site-partners') }}">Partners Page</a>
                </li>
            </ul>
        </li>
        <li class="{{ request()->is('dashboard/manage-data/*') ? ' open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                <i class="fa fa-database"></i>
                <span class="sidebar-mini-hide">Manage Data</span>
            </a>
            <ul>
                <li>
                    <a class="{{ request()->is('dashboard/manage-data/industries') ? ' active' : '' }}" href="{{ route('manage-data-industries') }}">Industries</a>
                </li>
                <li>
                    <a class="{{ request()->is('dashboard/manage-data/specializations') ? ' active' : ''}}" href="{{ route('manage-data-specializations') }}">Specializations</a>
                </li>
                <li>
                    <a class="{{ request()->is('dashboard/manage-data/insurances') ? ' active' : '' }}" href="{{ route('manage-data-insurances') }}">Insurances</a>
                </li>
            </ul>
        </li>
        <li>
            <a class="{{ request()->is('dashboard/users') ? ' active' : '' }}" href="{{ route('users') }}">
                <i class="fa fa-users"></i>
                <span class="sidebar-mini-hide">Users</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->is('dashboard/partners') ? ' active' : '' }}" href="{{ route('partners') }}">
                <i class="fa fa-handshake-o"></i>
                <span class="sidebar-mini-hide">Partners</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->is('dashboard/establishments') ? ' active' : '' }}" href="{{ route('establishments') }}">
                <i class="fa fa-building"></i>
                <span class="sidebar-mini-hide">Establishments</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->is('dashboard/advertisements') ? ' active' : '' }}" href="{{ route('advertisements') }}">
                <i class="fa fa-newspaper-o"></i>
                <span class="sidebar-mini-hide">Advertisements</span>
            </a>
        </li>
        <li>
            <a class="{{ request()->is('dashboard/newsletters') ? ' active' : '' }}" href="{{ route('newsletters') }}">
                <i class="fa fa-bullhorn"></i>
                <span class="sidebar-mini-hide">Newsletters</span>
            </a>
        </li>
        {{-- <li class="{{ request()->is('admin/location/*') ? ' open' : '' }}">
            <a class="nav-submenu" data-toggle="nav-submenu" href="#">
                <i class="fa fa-map-marker"></i>
                <span class="sidebar-mini-hide">Manage Locations</span>
            </a>
            <ul>
                <li>
                <a class="{{ request()->is('admin/location/add') ? ' active' : '' }}" href="{{ route('location.add') }}">Add Location</a>
                </li>
                <li>
                    <a class="{{ request()->is('admin/location/active') ? ' active' : '' }}" href="{{ route('location.active') }}">Active Locations</a>
                </li>
                <li>
                    <a class="{{ request()->is('admin/location/archived') ? ' active' : '' }}" href="{{ route('location.archived') }}">Archived Locations</a>
                </li>
                <li>
                    <a class="{{ request()->is('admin/location/verification') ? ' active' : '' }}" href="{{ route('location.verification') }}">Verification</a>
                </li>
            </ul>
        </li> --}}
        
    @else 
       
    @endif
</ul>