@isset($sec_feature)

<section class="feature-area bg-gray ptb--50" id="feature">
    <div class="container">
        <div class="section-title">
            <h2>{{ $sec_feature->title }}</h2>
            <p>{{ $sec_feature->sub_title }}</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="ft-content rtl">
                    @if ($sec_feature->feat_1_title)
                    <div class="ft-single" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        @isset($sec_feature->feat_1_icon)
                            <img src="{{ $sec_feature->feat_1_icon }}" alt="{{ $sec_feature->feat_1_title }}">
                        @endisset
                        <div class="meta-content">
                            <h2>{{ $sec_feature->feat_1_title }}</h2>
                            {!! $sec_feature->feat_1_content !!}
                        </div>
                    </div>
                    @endif
                    @if ($sec_feature->feat_3_title)
                    <div class="ft-single" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        @isset($sec_feature->feat_3_icon)
                            <img src="{{ $sec_feature->feat_3_icon }}" alt="{{ $sec_feature->feat_3_title }}">
                        @endisset
                        <div class="meta-content">
                            <h2>{{ $sec_feature->feat_3_title }}</h2>
                            {!! $sec_feature->feat_3_content !!}
                        </div>
                    </div>
                    @endif
                    @if ($sec_feature->feat_5_title)
                    <div class="ft-single" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        @isset($sec_feature->feat_5_icon)
                            <img src="{{ $sec_feature->feat_5_icon }}" alt="{{ $sec_feature->feat_5_title }}">
                        @endisset
                        <div class="meta-content">
                            <h2>{{ $sec_feature->feat_5_title }}</h2>
                            {!! $sec_feature->feat_5_content !!}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <div class="col-md-4 hidden-xs hidden-sm col-xs-12">
                <div class="ft-screen-img" data-aos="zoom-in" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @isset($sec_feature->main_image)
                        <img src="{{ $sec_feature->main_image }}" alt="image">
                    @endisset
                </div>
            </div>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <div class="ft-content">
                    @if ($sec_feature->feat_2_title)
                    <div class="ft-single" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        @isset($sec_feature->feat_2_icon)
                            <img src="{{ $sec_feature->feat_2_icon }}" alt="{{ $sec_feature->feat_2_title }}">
                        @endisset
                        <div class="meta-content">
                            <h2>{{ $sec_feature->feat_2_title }}</h2>
                            {!! $sec_feature->feat_2_content !!}
                        </div>
                    </div>
                    @endif
                    @if ($sec_feature->feat_4_title)
                    <div class="ft-single" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        @isset($sec_feature->feat_4_icon)
                            <img src="{{ $sec_feature->feat_4_icon }}" alt="{{ $sec_feature->feat_4_title }}">
                        @endisset
                        <div class="meta-content">
                            <h2>{{ $sec_feature->feat_4_title }}</h2>
                            {!! $sec_feature->feat_4_content !!}
                        </div>
                    </div>
                    @endif
                    @if ($sec_feature->feat_6_title)
                    <div class="ft-single" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        @isset($sec_feature->feat_6_icon)
                            <img src="{{ $sec_feature->feat_6_icon }}" alt="{{ $sec_feature->feat_6_title }}">
                        @endisset
                        <div class="meta-content">
                            <h2>{{ $sec_feature->feat_6_title }}</h2>
                            {!! $sec_feature->feat_6_content !!}
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</section>

@endisset