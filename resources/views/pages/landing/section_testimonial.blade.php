@isset($sec_testimonial)

<div class="testimonial-area ptb--50">
    <div class="container">
        <div class="section-title">
            <h2>{{ $sec_testimonial->title }}</h2>
            <p>{{ $sec_testimonial->sub_title }}</p>
        </div>
        <div class="testimonial-slider owl-carousel">
            @foreach ($testimonial_items as $item)
            <div class="single-tst">
                @isset($item->client_image)
                <img src="{{ $item->client_image }}" alt="{{ $item->client_name }}">
                @endisset
                {{-- <h4>{{ $item->client_name }}</h4> --}}
                @isset($item->client_image)
                {{-- <span>{{ $item->client_position }}</span> --}}
                @endisset
                <i><p>{{ $item->client_said }}</p></i>
                <h4 class="text-right">- {{ $item->client_name }}</h4>
                <i><h4 class="text-right"><small>{{ $item->client_position }}</small></h4></i>
                {{-- <ul class="tst-social">
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    <li><a href="#"><i class="fa fa-dribbble"></i></a></li>
                    <li><a href="#"><i class="fa fa-ponterest"></i></a></li>
                </ul> --}}
            </div>
            @endforeach
        </div>
    </div>
</div>

@endisset