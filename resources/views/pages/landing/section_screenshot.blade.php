@isset($sec_screenshot)
    <section class="screen-area bg-gray ptb--100" id="screenshot">
        <div class="container">
            <div class="section-title">
                <h2>{{ $sec_screenshot->title }}</h2>
                {{-- <p>{{ $sec_screenshot->sub_title }}</p> --}}
                <p id="screenshot_image_alt">{{ $screenshot_images[0]->image_alt }}</p>
            </div>
            
            <img class="screen-img" src="{{ $sec_screenshot->main_image }}" alt="mobile screen">
            <div class="screen-slider owl-carousel">

                @if (count($screenshot_images) > 0)
                    @foreach ($screenshot_images as $screenshot_image)
                        <div class="single-screen">
                            <img src="{{ $screenshot_image->image }}" alt="{{ $screenshot_image->image_alt }}" data-alt="{{ $screenshot_image->image_alt }}" class="small-2-50 w-100" data-aos="zoom-in" data-aos-offset="300" data-aos-easing="ease-in-sine">
                        </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
@endisset