@isset($sec_download)
    
<section class="call-to-action ptb--100" id="download" style="
    background-image: url({{ $sec_download->bg_image }});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    ">
    <div class="container">
        <div class="section-title text-white">
            <h2>{{ $sec_download->title }}</h2>
            <p>{{ $sec_download->sub_title }}</p>
        </div>
        <div class="download-btns btn-area text-center">
            <div>
                @isset($sec_download->ios_link)
                <a href="{{ $sec_download->ios_link }}" target="_blank">
                    <img src="{{ $sec_download->ios_image }}" alt="App Store" width="200" style="margin-top: 10px;">
                </a>
                @endisset
                @isset($sec_download->android_link)
                <a href="{{ $sec_download->android_link }}" target="_blank">
                    <img src="{{ $sec_download->android_image }}" alt="Play Store" width="200" style="margin-top: 10px;">
                </a>
                @endisset
            </div>
        </div>
    </div>
</section>

@endisset