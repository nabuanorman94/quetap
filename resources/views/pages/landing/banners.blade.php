<!-- slider area start -->
<section id="home" class="slider-area background-video">

    <div id="quetap_banner" class="carousel slide h-100 w-100" data-ride="carousel">
        @isset($banners)
        <!-- Indicators -->
        <ol class="carousel-indicators">
          <li data-target="#quetap_banner" data-slide-to="0" class="active"></li>
          @foreach ($banners as $banner)
          <li data-target="#quetap_banner" data-slide-to="{{ $loop->iteration }}"></li>
          @endforeach
        </ol>
        @endisset
        
        @isset($main_banner)
        <!-- Wrapper for slides -->
        <div class="carousel-inner h-100">
            @if($main_banner->bg_image)
                <div class="item h-100 active" style="
                    background-image: url( {{ $main_banner->bg_image }});
                    background-position: center;
                    background-repeat: no-repeat;
                    background-size: cover;
                ">
            @else
                <div class="item h-100 active">
            @endif
                <div class="container">
                    <div class="col-md-6 col-sm-6 hidden-xs">
                        <div class="row">
                            <div class="slider-img text-center">
                                @if ($main_banner->main_image)
                                    <img src="{{ $main_banner->main_image }}" alt="slider image" data-aos="zoom-in" data-aos-offset="300" data-aos-easing="ease-in-sine">
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="row" data-aos="zoom-in-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            <div class="slider-inner text-right">
                                <h1>
                                    <small class="heading1">{{ $main_banner->heading_1 }}</small>
                                    <small class="heading2">{{ $main_banner->heading_2 }}</small>
                                </h1>
                                {{-- <h1 data-aos="zoom-in-right" data-aos-offset="300" data-aos-easing="ease-in-sine">{{ $main_banner->heading_2 }}</h1> --}}
                                <h5>{{ $main_banner->sub_heading }}</h5>
                                @isset($main_banner->video_link)
                                <a class="expand-video" href="{{ $main_banner->video_link }}"><i class="fa fa-play"></i>Watch the video</a>
                                @endisset
                                <div class="download-app-buttons">
                                    @isset($sec_download->ios_link)
                                    <a href="{{ $sec_download->ios_link }}" target="_blank">
                                        <img src="{{ $sec_download->ios_image }}" alt="App Store" width="200" style="margin-top: 10px;">
                                    </a>
                                    @endisset
                                    @isset($sec_download->android_link)
                                    <a href="{{ $sec_download->android_link }}" target="_blank">
                                        <img src="{{ $sec_download->android_image }}" alt="Play Store" width="200" style="margin-top: 10px;">
                                    </a>
                                    @endisset
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endisset

            @isset($banners)
            @foreach ($banners as $banner)
                @if($banner->bg_image)
                    <div class="item h-100" style="
                        background-image: url( {{ $banner->bg_image }});
                        background-position: center;
                        background-repeat: no-repeat;
                        background-size: cover;
                    ">
                @else
                    <div class="item h-100">
                @endif
                    @if ($banner->orientation == 'left')
                        <div class="container">
                            <div class="col-md-6 col-sm-6 hidden-xs">
                                <div class="row" data-aos="zoom-in-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                                    <div class="slider-img text-center">
                                        @if ($banner->main_image)
                                            <img src="{{ $banner->main_image }}" alt="slider image">
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="slider-inner text-center">
                                        <h2>{{ $banner->heading }}</h2>
                                        <h5>{{ $banner->details }}</h5>
                                        @if ($banner->cta_link && $banner->cta_button_label)
                                            <a class="cta p-0" href="{{ $banner->cta_link }}">{{ $banner->cta_button_label }}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="container">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="slider-inner text-center">
                                        <h2>{{ $banner->heading }}</h2>
                                        <h5>{{ $banner->details }}</h5>
                                        @if ($banner->cta_link && $banner->cta_button_label)
                                            <a class="cta p-0" href="{{ $banner->cta_link }}">{{ $banner->cta_button_label }}</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 hidden-xs">
                                <div class="row">
                                    <div class="slider-img text-center">
                                        @if ($banner->main_image)
                                            <img src="{{ $banner->main_image }}" alt="slider image">
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            @endforeach
            @endisset

    </div>

</section>
<!-- slider area end -->