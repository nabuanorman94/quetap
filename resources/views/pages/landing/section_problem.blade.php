@isset($sec_problem)
    
<div class="about-area ptb--60">
    <div class="container">
        <div class="section-title">
            <h2>{{ $sec_problem->title }}</h2>
            <p>{{ $sec_problem->sub_title }}</p>
        </div>
        <div class="row d-flex flex-center">
            <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                <div class="about-content" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @isset ($sec_problem->content)
                    {!! $sec_problem->content !!}
                    @endisset
                </div>
            </div>
            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="about-left-img">
                    @isset ($sec_problem->main_image)
                    <img src="{{ $sec_problem->main_image }}" class="w-100 mb-2" alt="" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @endisset
                </div>
            </div>
        </div>

        <div class="row d-flex flex-center">
            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="about-left-img">
                    @isset ($sec_problem->main_image_2)
                    <img src="{{ $sec_problem->main_image_2 }}" class="w-100 mb-2" alt=""  data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @endisset
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                <div class="about-content"data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @isset ($sec_problem->content_2)
                    {!! $sec_problem->content_2 !!}
                    @endisset
                </div>
            </div>
        </div>

        <div class="row d-flex flex-center">
            <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                <div class="about-content" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @isset ($sec_problem->content_3)
                    {!! $sec_problem->content_3 !!}
                    @endisset
                </div>
            </div>
            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="about-left-img">
                    @isset ($sec_problem->main_image_3)
                    <img src="{{ $sec_problem->main_image_3 }}" class="w-100 mb-2" alt="" data-aos="fade-up" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @endisset
                </div>
            </div>
        </div>
    </div>
</div>

@endisset