@isset($sec_contact_us)
<section class="contact-area ptb--30" id="contact">
    <div class="container">
        <div class="section-title">
            <h2>{{ $sec_contact_us->title }}</h2>
            <p>{{ $sec_contact_us->sub_title }}</p>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="contact-form">

                    <small class="text-left" id="contact-us-success"></small>

                    <form action="{{ route('contact-us') }}" method="POST" id="contactForm">
                        @csrf
                        <input type="text" name="name" placeholder="Enter Your Name" required>
                        <input type="email" name="email" placeholder="Enter Your Email" required>
                        <textarea name="message" id="cu_message" placeholder="Your Message " required></textarea>
                        <input type="submit" value="Send" id="send">
                    </form>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="contact_info">
                    @isset($sec_contact_us->address)
                    <div class="s-info">
                        <i class="fa fa-map-marker"></i>
                        <div class="meta-content">
                            <span>{{ $sec_contact_us->address }}</span>
                        </div>
                    </div>
                    @endisset
                    @isset($sec_contact_us->contact_number)
                    <div class="s-info">
                        <i class="fa fa-mobile"></i>
                        <div class="meta-content">
                            <span>{{ $sec_contact_us->contact_number }}</span>
                        </div>
                    </div>
                    @endisset
                    @isset($sec_contact_us->email)
                    <div class="s-info">
                        <i class="fa fa-paper-plane"></i>
                        <div class="meta-content">
                            <span>{{ $sec_contact_us->email }}</span>
                        </div>
                    </div>
                    @endisset
                    <div class="c-social">
                        <ul>
                            @isset($sec_contact_us->facebook_link)
                            <li><a href="{{ $sec_contact_us->facebook_link }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                            @endisset
                            @isset($sec_contact_us->twitter_link)
                            <li><a href="{{ $sec_contact_us->twitter_link }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            @endisset
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endisset