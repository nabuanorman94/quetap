@isset($sec_counter)

<div class="achivement-area ptb--100" style="
    background-image: url({{ $sec_counter->bg_image }});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    ">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="ach-single">
                    <div class="icon"><i class="{{ $sec_counter->count_1_icon }}"></i></div>
                    <p><span class="counter">{{ $sec_counter->count_1_value }}</span>k</p>
                    <h5>{{ $sec_counter->count_1_title }}</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="ach-single">
                    <div class="icon"><i class="{{ $sec_counter->count_2_icon }}"></i></div>
                    <p><span class="counter">{{ $sec_counter->count_2_value }}</span>k</p>
                    <h5>{{ $sec_counter->count_2_title }}</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="ach-single">
                    <div class="icon"><i class="{{ $sec_counter->count_3_icon }}"></i></div>
                    <p><span class="counter">{{ $sec_counter->count_3_value }}</span>k</p>
                    <h5>{{ $sec_counter->count_3_title }}</h5>
                </div>
            </div>
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="ach-single">
                    <div class="icon"><i class="{{ $sec_counter->count_4_icon }}"></i></div>
                    <p><span class="counter">{{ $sec_counter->count_4_value }}</span>k</p>
                    <h5>{{ $sec_counter->count_4_title }}</h5>
                </div>
            </div>
        </div>
    </div>
</div>

@endisset