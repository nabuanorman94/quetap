@isset($sec_solution)

<div class="about-area ptb--60">
    <div class="container">
        <div class="section-title">
            <h2>{{ $sec_solution->title }}</h2>
            <p>{{ $sec_solution->sub_title }}</p>
        </div>
        <div class="row d-flex flex-center">

            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="about-left-img">
                    @isset ($sec_solution->main_image)
                        <img src="{{ $sec_solution->main_image }}" class="w-100 mb-2" alt="" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                    @endisset
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                <div class="about-content" data-aos="fade-left" data-aos-offset="200" data-aos-easing="ease-in-sine">
                    {!! $sec_solution->content !!}
                </div>
            </div>
            
        </div>
    </div>
</div>

@endisset