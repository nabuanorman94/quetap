@extends('layouts.backend')

@inject('admin', 'App\Http\Controllers\AdminController')

@section('content')
    <div id="app" class="content">
        <div class="row">
            
            <div class="col-sm-12 col-md-6 col-lg-3">
                <a class="block block-link-pop text-right bg-primary" href="#">
                    <div class="block-content block-content-full clearfix border-black-op-b border-3x">
                        <div class="float-left mt-10">
                            <i class="fa fa-users fa-2x text-info-light"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white" data-toggle="countTo" data-speed="500" data-to="10">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-white-op">Total Users</div>
                    </div>
                </a>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-3">
                <a class="block block-link-pop text-right bg-warning" href="#">
                    <div class="block-content block-content-full clearfix border-black-op-b border-3x">
                        <div class="float-left mt-10">
                            <i class="fa fa-building fa-2x text-info-light"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white" data-toggle="countTo" data-speed="500" data-to="20">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-white-op">Establishments</div>
                    </div>
                </a>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-3">
                <a class="block block-link-pop text-right bg-earth" href="#">
                    <div class="block-content block-content-full clearfix border-black-op-b border-3x">
                        <div class="float-left mt-10">
                            <i class="fa fa-list-ol fa-2x text-info-light"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white" data-toggle="countTo" data-speed="500" data-to="30">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-white-op">Queues</div>
                    </div>
                </a>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-3">
                <a class="block block-link-pop text-right bg-info" href="#">
                    <div class="block-content block-content-full clearfix border-black-op-b border-3x">
                        <div class="float-left mt-10">
                            <i class="fa fa-bullhorn fa-2x text-info-light"></i>
                        </div>
                        <div class="font-size-h3 font-w600 text-white" data-toggle="countTo" data-speed="500" data-to="40">0</div>
                        <div class="font-size-sm font-w600 text-uppercase text-white-op">Newsletters</div>
                    </div>
                </a>
            </div>
        </div>
    </div>
@endsection
