<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-172033991-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-172033991-1');
        </script>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>@yield('title') QueTap - Queue Safely, Queue Online</title>
        <meta name="description" content="Quetap is an innovative application that allows you to queue and keep track of your serving status online eliminating the hassle of physically lining.">
        <meta name="url" content="https://quetap.ph/">
        @yield('meta-tags')
        <meta property="og:url"                content="http:quetap.ph" />
        <meta property="og:type"               content="website" />
        <meta property="og:title"              content="QueTap - Queue Safely, Queue Online" />
        <meta property="og:description"        content="Quetap is an innovative application that allows you to queue and keep track of your serving status online eliminating the hassle of physically lining which saves you a lot of quality time, effort and money." />
        <meta property="og:image"              content="https://quetap-site-bucket.s3.ap-northeast-1.amazonaws.com/site/section-features/cpdbIDRePq36JpjbpP2CCrQepkQ8wNRYBgxB41pj.png" />

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.ico') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        <!-- Fonts and Styles -->
        @yield('css_before')

        <link rel="stylesheet" id="css-main" href="{{ asset('css/landing/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/owl.carousel.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/slicknav.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/magnificpopup.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/jquery.mb.YTPlayer.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/typography.css') }}">
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/> --}}
        <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
        <link rel="stylesheet" id="css-main" href="{{ asset('css/landing/style.css') }}">
        <link rel="stylesheet" id="css-main" href="{{ asset('css/landing/responsive.css') }}">

        @yield('css_after')

        <style>
            .screen-slider .owl-item:not(.center) {
                opacity: 0;
            }
        </style>

        @php
            $pages = App\Page::all();    
        @endphp

    </head>
    <body>
        <!-- preloader area start -->
        {{-- <div id="preloader">
            <div class="spinner"></div>
        </div> --}}
        <!-- preloader area end -->
        <!-- header area start -->
        <header id="header">
            <div class="header-area">
                <div class="container">
                    <div class="row">
                        <div class="menu-area">
                            <div class="col-md-3 col-sm-12 col-xs-12">
                                <div class="logo">
                                    <a href="{{ route('landing-page') }}"><img src="{{ asset('media/images/logo3.png') }}" width="100" alt="QueTap"></a>
                                </div>
                            </div>
                            <div class="col-md-9 hidden-xs hidden-sm">
                                <div class="main-menu">
                                    <nav class="nav-menu">
                                        <ul>
                                            @if (request()->is('/'))
                                            
                                            <li class="active"><a href="#header">Home</a></li>
                                            <li><a href="#feature">Features</a></li>
                                            <li><a href="#screenshot">How it works</a></li>
                                            <li><a href="#download">Download</a></li>
                                            <li><a href="#contact">Contact</a></li>
                                            <li><a href="{{ route('partners-page') }}">Become a Partner</a></li>

                                            @else

                                            <li><a href="{{ route('landing-page') }}">Home</a></li>
                                            @foreach ($pages as $page)
                                            <li class="{{ request()->path() == $page->slug ? 'active' : '' }}"><a href="{{ $page->slug }}">{{ $page->title }}</a></li>
                                            @endforeach
                                            <li><a href="{{ route('partners-page') }}">Become a Partner</a></li>

                                            @endif
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="col-sm-12 col-xs-12 visible-sm visible-xs">
                                <div class="mobile_menu"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- header area end -->
        
        <div id="page-container" class="main-content-boxed">
            <!-- Main Container -->
            <main id="main-container">
                @yield('content')
            </main>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- footer area start -->
        <footer>
            <div class="footer-area">
                <div class="container">
                    <div class="row">
                        <div class="col-12 col-sm-6 col-md-10">
                            <ul>
                                @foreach ($pages as $page)
                                <li><a href="{{ $page->slug }}">{{ $page->title }}</a></li>
                                @endforeach
                                <li><a href="{{ route('partners-page') }}">Become a Partner</a></li>
                            </ul>
                        </div>
                        <div class="col-12 col-sm-6 col-md-2">
                            <p>Copyright &copy;<script>document.write(new Date().getFullYear());</script></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer area end -->

        <!-- Scripts -->
        <script src="{{ asset('js/landing/jquery-3.2.0.min.js') }}"></script>
        <script src="{{ asset('js/landing/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/landing/bootstrap.min.js') }}"></script>
        <script src="{{ asset('js/landing/jquery.slicknav.min.js') }}"></script>
        <script src="{{ asset('js/landing/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('js/landing/magnific-popup.min.js') }}"></script>
        <script src="{{ asset('js/landing/counterup.js') }}"></script>
        <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
        <script src="{{ asset('js/landing/jquery.waypoints.min.js') }}"></script>
        <script src="{{ asset('js/landing/jquery.mb.YTPlayer.min.js') }}"></script>
        <script src="{{ asset('js/landing/theme.js') }}"></script>

        <script>
            AOS.init();
        </script>

        @yield('js_after')
    </body>
</html>
