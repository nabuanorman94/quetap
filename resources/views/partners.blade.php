@extends('layouts.simple')

@section('title')
    Become a QueTap Partner |
@endsection

@section('js_after')
    <script>
        $("#contactForm").submit(function(e) {

            e.preventDefault();

            $('#send').attr({
                disabled: true,
                style: 'background-color: #444;',
                value: 'Sending...'
            });

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(response)
                {
                    $('input[name="name"]').val('');
                    $('input[name="email"]').val('');
                    $('#cu_message').val('');
                    $('#contact-us-success').html('<i class="fa fa-check"></i> '+response.success);
                    $('#send').attr('value', 'Sent');
                }
            });
        });
    </script>
@endsection

@section('content')
    <div class="about-area ptb--60">
        <div class="container">
            <div class="section-title ptb--50">
                <h2>Be a QueTap Partner!</h2>
                <p>What's in it for you?</p>
            </div>

            @foreach ($partners_page_details as $item)
            <div class="row d-flex flex-center">
                
                @if(($loop->iteration % 2) != 0)
                    <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                        <div class="about-content">
                            {!! $item->content !!}
                        </div>
                    </div>
        
                    <div class="col-md-6 col-sm-6 hidden-xs">
                        <div class="about-left-img">
                            @isset ($item->image)
                                <img src="{{ $item->image }}" class="w-100 mb-2" alt="" data-aos="fade-left" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            @endisset
                        </div>
                    </div>
                @else 
                    <div class="col-md-6 col-sm-6 hidden-xs">
                        <div class="about-left-img">
                            @isset ($item->image)
                                <img src="{{ $item->image }}" class="w-100 mb-2" alt="" data-aos="fade-right" data-aos-offset="300" data-aos-easing="ease-in-sine">
                            @endisset
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                        <div class="about-content">
                            {!! $item->content !!}
                        </div>
                    </div>
                @endif
    
            </div>
            @endforeach
        </div>
    </div>

    @include('pages.landing.section_contact_us')

@endsection