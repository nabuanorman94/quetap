@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

Thank you for subscibing to QueTap mailing list!

{{-- @component('mail::button', ['url' => ''])
Button Text
@endcomponent --}}
<br>
Regards,<br>
{{-- {{ config('app.name') }} --}}
QueTap Team

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
