@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

Hi Admin, <b>{{ $email }}</b> just signed up to our mailing list.

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
