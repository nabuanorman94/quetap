@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

New Partner Signup
<br>

Name: {{ $partner->first_name . ' ' . $partner->middle_name . ' ' . $partner->last_name }} <br>
Email: {{ $partner->email }} <br>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent