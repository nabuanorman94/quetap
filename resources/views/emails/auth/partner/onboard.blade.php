@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

Hi {{ $user->first_name }},
<br><br>
{{-- {{ $message }} --}}
Congratulations! Your Quetap Partner Account has been set up. You can now log-in to our partner's dashboard and start creating online queues for your establishments.
<br>
Click <a href="http://partners.quetap.ph/">here</a> to login.
<br>

<br><br>
Regards,<br>
{{-- {{ config('app.name') }} --}}
QueTap Team

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent