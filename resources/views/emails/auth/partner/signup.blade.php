@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

Hi {{ $user->first_name }},
<br>
{{-- {{ $mesage }} --}}
Thank you for being a part of QueTap's growing partners. Please expect a call from our Quetap Partner Specialist within the next 24 hours.

{{-- @component('mail::button', ['url' => $link])
Confirm Email
@endcomponent

If the button doesn't work, click the link instead, <a href="{{ $link }}">{{ $link }}</a> --}}


<br><br>
Regards,<br>
{{-- {{ config('app.name') }} --}}
QueTap Team

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent