@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

Hi {{ $user->name }},
<br>
{{-- {{ $mesage }} --}}
Thank you for reaching on us. We will get back to you as soon as possible.
<br><br>
Regards,<br>
{{-- {{ config('app.name') }} --}}
QueTap Team

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
