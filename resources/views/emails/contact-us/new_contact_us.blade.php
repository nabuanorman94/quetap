@component('mail::layout')
@slot('header')
    @component('mail::header', ['url' => config('app.url')])
        <img src="{{ url('/media/images/logo.png') }}" alt="queTap" style="width: 80px;">
    @endcomponent
@endslot

Hi Admin, {{ $user->name }} just contacted us.
<br>
<p>Name: <b>{{ $user->name }}</b></p>
<p>Email: <b>{{ $user->email }}</b></p>
<p>Message: <b>{{ $user->message }}</b></p>

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot

@endcomponent
