@extends('layouts.simple')

@section('css_after')
<link rel="stylesheet" href="{{ asset('js/plugins/sweetalert2/sweetalert2.min.css') }}">
@endsection

@section('js_after')
    <script src="{{ asset('js/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
    <script>
        $("#contactForm").submit(function(e) {

            e.preventDefault();

            $('#send').attr({
                disabled: true,
                style: 'background-color: #444;',
                value: 'Sending...'
            });

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(response)
                {
                    $('input[name="name"]').val('');
                    $('input[name="email"]').val('');
                    $('#cu_message').val('');
                    $('#contact-us-success').html('<i class="fa fa-check"></i> '+response.success);
                    $('#send').attr('value', 'Sent');

                    swal.fire({
                        title: "Message Sent",
                        text: "Thank you for reaching on us. We will get back to you as soon as possible.",
                        icon: "success",
                        showCancelButton: !1,
                        buttonsStyling: !1,
                        customClass: {
                            confirmButton: "btn btn-alt-success m-5",
                            input: "form-control"
                        },
                        html: !1,
                        allowOutsideClick: false,
                    });
                }
            });
        });

        $('.owl-dot').on('click', function(e) {
            setTimeout(() => {
                var image = $('.screen-slider').find('div.owl-item.center img');
                $('#screenshot_image_alt').html($(image).data('alt'));
            }, 500);
        });
    </script>
@endsection

@section('content')
    
    @include('pages.landing.banners')
    
    @include('pages.landing.section_problem')

    @include('pages.landing.section_solution')

    @include('pages.landing.section_features')

    @include('pages.landing.section_counter')
    
    @include('pages.landing.section_screenshot')

    @include('pages.landing.section_download')
    
    @include('pages.landing.section_testimonial')

    @include('pages.landing.section_contact_us')

@endsection
