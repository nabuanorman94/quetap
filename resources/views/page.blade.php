@extends('layouts.simple')

@isset($page)

@section('title')
    {{ $page->title }} |
@endsection

@section('js_after')
    <script>
        $("#contactForm").submit(function(e) {

            e.preventDefault();

            $('#send').attr({
                disabled: true,
                style: 'background-color: #444;',
                value: 'Sending...'
            });

            var form = $(this);
            var url = form.attr('action');

            $.ajax({
                type: "POST",
                url: url,
                data: form.serialize(),
                success: function(response)
                {
                    $('input[name="name"]').val('');
                    $('input[name="email"]').val('');
                    $('#cu_message').val('');
                    $('#contact-us-success').html('<i class="fa fa-check"></i> '+response.success);
                    $('#send').attr('value', 'Sent');
                }
            });
        });
    </script>
@endsection

@section('content')
    
<section class="call-to-action ptb--100 page-banner" style="
    background-image: url({{ $page->bg_image }});
    background-position: center;
    background-repeat: no-repeat;
    background-size: cover;
    ">
    <div class="container">
        <div class="section-title text-white">
            <h2>{{ $page->title }}</h2>
        </div>
    </div>
</section>

@isset($page->main_content)
<div class="about-area pt--50">
    <div class="container">
        <div class="row d-flex flex-center">
            <div class="col-md-12 d-flex flex-center">
                <div class="about-content">
                    {!! $page->main_content !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endisset

@isset($page->content)
<div class="about-area pt--50">
    <div class="container">
        <div class="row d-flex flex-center">
            
            <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                <div class="about-content">
                    {!! $page->content !!}
                </div>
            </div>

            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="about-left-img">
                    @isset ($page->main_image)
                        <img src="{{ $page->main_image }}" class="w-100 mb-2" alt="">
                    @endisset
                </div>
            </div>

        </div>
    </div>
</div>
@endisset

@isset($page->content_2)
<div class="about-area ptb--50">
    <div class="container">
        <div class="row d-flex flex-center">

            <div class="col-md-6 col-sm-6 hidden-xs">
                <div class="about-left-img">
                    @isset ($page->main_image_2)
                        <img src="{{ $page->main_image_2 }}" class="w-100 mb-2" alt="">
                    @endisset
                </div>
            </div>

            <div class="col-md-6 col-sm-6 col-xs-12 d-flex flex-center">
                <div class="about-content">
                    {!! $page->content_2 !!}
                </div>
            </div>

        </div>
    </div>
</div>
@endisset

@endisset

@isset($sec_contact_us)
@include('pages.landing.section_contact_us')
@endisset
    
@endsection
