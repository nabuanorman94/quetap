<!doctype html>
<html lang="{{ config('app.locale') }}" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>QueTap - Queue Safely, Queue Online</title>

        <meta name="description" content="QueTap - Queue Safely, Queue Online">
        <meta name="author" content="norman">
        <meta name="robots" content="noindex, nofollow">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Icons -->
        <link rel="shortcut icon" href="{{ asset('media/favicons/favicon.ico') }}">
        <link rel="icon" sizes="192x192" type="image/png" href="{{ asset('media/favicons/favicon-192x192.png') }}">
        <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('media/favicons/apple-touch-icon-180x180.png') }}">

        <!-- Fonts and Styles -->
        @yield('css_before')

        <link rel="stylesheet" id="css-main" href="{{ asset('css/landing/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/jquery-ui.css') }}">
        <link rel="stylesheet" href="{{ asset('css/landing/font-awesome.min.css') }}">
        <link rel="stylesheet" id="css-main" href="{{ asset('css/landing/style.css') }}">
        <link rel="stylesheet" id="css-main" href="{{ asset('css/landing/responsive.css') }}">

        @yield('css_after')

        <!-- Scripts -->
        <script>window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};</script>
    </head>
    <body>
        <!-- preloader area start -->
        <div id="preloader">
            <div class="spinner"></div>
        </div>
        <!-- preloader area end -->
        
        
        <div id="page-container" class="main-content-boxed bg-primary full-height">
            <!-- Main Container -->
            <div class="container-fluid">
                <div class="row" style="margin-top: 100px;">
                    <div class="col-sm-12" id="section">
                        <div class="text-center">
                            <img src="{{ asset('media/images/logo-inverse.png') }}" width="100" alt="QueTap">
                            <h1>Queuing Soon</h1>
                            <p>Queue Safely, Queue Online</p>
                        </div>
                        <div class="row" id="contact">
                            <div class="col-sm-12 col-md-4 col-md-offset-4">
                                <div class="contact-form">
                                    <form action="{{ route('subscribe') }}" method="POST">
                                        @csrf
                                        <div class="form-row">
                                            <div class="col-sm-12">
                                                @if ($message = Session::get('success'))
                                                    <small class="text-left"><i class="fa {{ Session::get('icon') }}"></i> {{ $message }}</small>
                                                @else 
                                                    <small class="text-left"><i class="fa fa-info-circle"></i> Be the first to get the app! Subscribe to our mailing list.</small>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-row">
                                            <div class="col-md-9">
                                                <input type="email" name="email" placeholder="Enter Your Email" required>
                                            </div>
                                            <div class="col-md-3 text-center">
                                                <button type="submit" class="btn btn-lg btn-warning btn-block">
                                                    <i class="fa fa-paper-plane"></i>
                                                    Send
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Main Container -->
        </div>
        <!-- END Page Container -->

        <!-- Scripts -->
        <script src="{{ asset('js/landing/jquery-3.2.0.min.js') }}"></script>
        <script src="{{ asset('js/landing/jquery-ui.js') }}"></script>
        <script src="{{ asset('js/landing/bootstrap.min.js') }}"></script>

        @yield('js_after')
    </body>
</html>
