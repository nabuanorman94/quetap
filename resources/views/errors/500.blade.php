@extends('layouts.auth')

@section('title')
    Server Error |
@endsection

@section('content')
    <div id="app" class="main-content-boxed">

        <!-- Main Container -->
        <main id="main-container">
            <!-- Page Content -->
            <div class="bg-body-dark bg-pattern">
                <div class="row mx-0 justify-content-center">
                    <div class="hero-static col-lg-6 col-xl-4">
                        <div class="content content-full overflow-hidden">
                            <!-- Header -->
                            <div class="py-30 text-center">
                                <a class="link-effect font-w700" href="/">
                                    <img src="{{ url('/media/images/qt-icon.png') }}" alt="QueTap" class="logo-icon">
                                    <span class="d-inline-block">
                                        <span class="font-size-xl text-primary">UETAP</span>
                                    </span>
                                </a>
                            </div>
                            <!-- END Header -->

                            <div class="text-center">
                                <img src="{{ url('/media/images/500 Internal Server Error-amico.png') }}" alt="" class="w-100">
                                
                                <a class="link-effect font-w700" href="/">
                                    <span class="d-inline-block">
                                        <span class="font-size-xl text-primary"><i class="fa fa-arrow-left"></i> Return to Homepage</span>
                                    </span>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- END Page Content -->
        </main>
        <!-- END Main Container -->
    </div>
    <!-- END Page Container -->
@endsection