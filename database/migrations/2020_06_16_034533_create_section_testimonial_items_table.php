<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionTestimonialItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_testimonial_items', function (Blueprint $table) {
            $table->id();
            $table->string('client_name')->nullable();
            $table->string('client_position')->nullable();
            $table->string('client_image')->nullable();
            $table->text('client_said')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_testimonial_items');
    }
}
