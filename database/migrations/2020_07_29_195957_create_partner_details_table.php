<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePartnerDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('partner_id');
            $table->string('business_type');
            $table->string('business_name');
            $table->string('representative');
            $table->string('position');
            $table->string('contact_no');
            $table->string('business_address_street');
            $table->string('business_address_province');
            $table->string('business_address_city');
            $table->string('business_address_zip_code');
            $table->string('industry');
            $table->string('specialization')->nullable();
            $table->string('insurance')->nullable();
            $table->string('logo');
            $table->string('business_permit');
            $table->string('dti_sec_cert')->nullable();
            $table->string('bir_cert')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_details');
    }
}
