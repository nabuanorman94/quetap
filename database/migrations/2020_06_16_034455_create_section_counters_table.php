<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_counters', function (Blueprint $table) {
            $table->id();
            $table->string('bg_image')->nullable();
            $table->string('count_1_title')->nullable();
            $table->string('count_1_icon')->nullable();
            $table->integer('count_1_value')->nullable();
            $table->string('count_2_title')->nullable();
            $table->string('count_2_icon')->nullable();
            $table->integer('count_2_value')->nullable();
            $table->string('count_3_title')->nullable();
            $table->string('count_3_icon')->nullable();
            $table->integer('count_3_value')->nullable();
            $table->string('count_4_title')->nullable();
            $table->string('count_4_icon')->nullable();
            $table->integer('count_4_value')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_counters');
    }
}
