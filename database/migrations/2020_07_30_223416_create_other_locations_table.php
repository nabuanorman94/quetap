<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtherLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('other_locations', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('partner_id');
            $table->string('business_address_street');
            $table->string('business_address_province');
            $table->string('business_address_city');
            $table->string('business_address_zip_code');
            $table->string('contact_no');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('other_locations');
    }
}
