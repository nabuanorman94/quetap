<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterSectionProblem extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('section_problems', function (Blueprint $table) {
            $table->text('content_2')->after('main_image')->nullable();
            $table->string('main_image_2')->after('content_2')->nullable();
            $table->text('content_3')->after('main_image_2')->nullable();
            $table->string('main_image_3')->after('content_3')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('section_problems', function (Blueprint $table) {
            //
        });
    }
}
