<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiteMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('site_messages', function (Blueprint $table) {
            $table->id();
            $table->string('new_newsletter')->nullable();
            $table->string('new_newsletter_admin')->nullable();
            $table->string('new_contact_us')->nullable();
            $table->string('new_contact_us_admin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('site_messages');
    }
}
