<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionFeaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('section_features', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->string('main_image')->nullable();
            $table->string('feat_1_title')->nullable();
            $table->string('feat_1_content')->nullable();
            $table->string('feat_1_icon')->nullable();
            $table->string('feat_2_title')->nullable();
            $table->string('feat_2_content')->nullable();
            $table->string('feat_2_icon')->nullable();
            $table->string('feat_3_title')->nullable();
            $table->string('feat_3_content')->nullable();
            $table->string('feat_3_icon')->nullable();
            $table->string('feat_4_title')->nullable();
            $table->string('feat_4_content')->nullable();
            $table->string('feat_4_icon')->nullable();
            $table->string('feat_5_title')->nullable();
            $table->string('feat_5_content')->nullable();
            $table->string('feat_5_icon')->nullable();
            $table->string('feat_6_title')->nullable();
            $table->string('feat_6_content')->nullable();
            $table->string('feat_6_icon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('section_features');
    }
}
