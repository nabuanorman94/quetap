<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LandingPageSEO extends Model
{
    protected $fillable = [ 
        'title',
        'meta_tags',
    ];
}
