<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionScreenshot extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title',
        'main_image'
    ];
}
