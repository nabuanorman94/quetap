<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [ 
        'title',
        'slug',
        'bg_image',
        'meta_tags',
        'main_content',
        'content',
        'main_image',
        'content_2',
        'main_image_2'
    ];

    public function getRouteKeyName(): string {
        return 'slug';
    }
}
