<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OtherLocation extends Model
{
    protected $fillable = [ 
        'partner_id',
        'business_address_street',
        'business_address_province',
        'business_address_city',
        'business_address_zip_code',
        'contact_no',
    ];

    public function partner() {
        return $this->hasOne('App\Partner');
    }
}
