<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = [
        'bg_image',
        'main_image',
        'heading',
        'details',
        'cta_link',
        'cta_button_label',
        'orientation'
    ];
}
