<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    protected $fillable = [ 
        'industry_id',
        'name'
    ];

    public function industry() {
        return $this->belongsTo('App\Industry');
    }
}
