<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionProblem extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title',
        'content',
        'main_image',
        'content_2',
        'main_image_2',
        'content_3',
        'main_image_3',
        'video'
    ];
}
