<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteMessages extends Model
{
    protected $fillable = [ 
        'new_newsletter',
        'new_newsletter_admin',
        'new_contact_us',
        'new_contact_us_admin'
    ];
}
