<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionDownload extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title',
        'bg_image',
        'ios_image',
        'ios_link',
        'android_image',
        'android_link'
    ];
}
