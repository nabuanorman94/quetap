<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model
{
    protected $fillable = [ 
        'first_name',
        'middle_name',
        'last_name',
        'email',
        'contact_no',
        'time_from',
        'time_to',
        'password',
        'verified',
        'token',
        'onboarded'
    ];

    public function partnerDetails() {
        return $this->hasOne('App\PartnerDetails');
    }
}
