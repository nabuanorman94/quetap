<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerPageDetails extends Model
{
    protected $fillable = [ 
        'content',
        'image'
    ];
}
