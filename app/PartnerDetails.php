<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartnerDetails extends Model
{
    protected $fillable = [ 
        'partner_id',
        'business_type',
        'business_name',
        'representative',
        'position',
        'contact_no',
        'business_address_street',
        'business_address_province',
        'business_address_city',
        'business_address_zip_code',
        'industry',
        'specialization',
        'insurance',
        'logo',
        'business_permit',
        'dti_sec_cert',
        'bir_cert',
        'additional_details',
        'lat_long'
    ];

    public function partner() {
        return $this->hasOne('App\Partner');
    }
}
