<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionSolution extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title',
        'content',
        'main_image',
        'video'
    ];
}
