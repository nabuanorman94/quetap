<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use App\Traits\S3Traits;
use App\Traits\AdminTraits;

class AdminController extends Controller
{
    use S3Traits, AdminTraits;

    public function __construct()
    {
        // $this->middleware(['auth']);
    }

    public function index()
    {
        return view('dashboard');
    }

    public function advertisements() {
        return view('dashboard.admin.advertisements');
    }

    public function users() {
        return view('dashboard.admin.users');
    }

    public function partners() {
        return view('dashboard.admin.partners')->with([
            'partners' => $this->getPartners()
        ]);
    }

    public function establishments() {
        return view('dashboard.admin.establishments');
    }

    public function newsletters() {
        return view('dashboard.admin.newsletters');
    }

    public function manageSiteLanding() {
        return view('dashboard.admin.manage_site_landing')->with($this->getRequiredData());
    }

    public function manageSiteOther() {
        return view('dashboard.admin.manage_site_other')->with([
            'pages' => $this->getOtherPages()
        ]);
    }

    public function manageSiteMessages() {
        return view('dashboard.admin.manage_site_messages');
    }

    public function manageSitePartners() {
        return view('dashboard.admin.manage_site_partners')->with([
            'partners_page_details' => $this->getPartnersPageDetails()
        ]);
    }
    
    // Other Pages | Add Page
    public function addPage(Request $request) {
        $rules = [
            'title' => ($request->page_id)? 'required' : 'required|unique:pages',
            'slug' => ($request->page_id)? 'required' : 'required|unique:pages',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            'slug.required' => 'Slug is required.'
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Background Image
        if($request->hasFile('bg_image')) {
            $bg_image = new Collection();
            $bg_image->folder = config('app.aws_bucket_folder').'/pages'.'/'.$request->slug.'/bg-image';
            $bg_image->file = $request->bg_image;
            $bg_image_path = $this->uploadImageToS3($bg_image);
        }

        // Main Image 1
        if($request->hasFile('main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/pages'.'/'.$request->slug.'/images';
            $main_image->file = $request->main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // Main Image 2
        if($request->hasFile('main_image_2')) {
            $main_image_2 = new Collection();
            $main_image_2->folder = config('app.aws_bucket_folder').'/pages'.'/'.$request->slug.'/images';
            $main_image_2->file = $request->main_image_2;
            $main_image_2_path = $this->uploadImageToS3($main_image_2);
        }

        // To save Banner
        $page = array();
        isset($bg_image_path)? $page['bg_image'] = $bg_image_path : null;
        isset($main_image_path)? $page['main_image'] = $main_image_path :null;
        isset($main_image_2_path)? $page['main_image_2'] = $main_image_2_path :null;
        isset($request->page_id)? $page['id'] = $request->page_id :null;
        $page['title'] = $request->title;
        $page['slug'] = $request->slug;
        $page['meta_tags'] = $request->meta_tags;
        $page['main_content'] = $request->main_content;
        $page['content'] = $request->content;
        $page['content_2'] = $request->content_2;

        if(!$request->page_id) {
            $this->pageAdd($page);
            return redirect()->route('manage-site-other-pages')->with([
                'success' => 'Page added successfully!'
            ]);
        } else {
            $this->pageUpdate($page);
            return redirect()->route('manage-site-other-pages')->with([
                'success' => 'Page updated successfully!'
            ])->withInput();
        }
    }

    public function addPageView() {
        return view('dashboard.admin.manage_site_other_add');
    }

    public function editPageView($id) {
        return view('dashboard.admin.manage_site_other_add')->with([
            'page' => $this->getPageToEdit($id)
        ]);
    }

    public function removePage(Request $request) {
        $this->pageRemove($request);
        return back()->with([
            'success' => 'Page removed!'
        ]);
    }

    // Site Management
    public function addBanner(Request $request) {

        $rules = [
            'banner_heading' => 'required',
            // 'banner_details' => 'required',
            // 'banner_bg_image' => (!$request->banner_id)? 'required' : '',
        ];

        $messages = [
            'banner_heading.required' => 'Heading is required.',
            // 'banner_details.required' => 'Details is required.',
            // 'banner_bg_image.required' => 'Background image is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('banner_main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/banners/main-images';
            $main_image->file = $request->banner_main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // Background Image
        if($request->hasFile('banner_bg_image')) {
            $bg_image = new Collection();
            $bg_image->folder = config('app.aws_bucket_folder').'/banners/bg-images';
            $bg_image->file = $request->banner_bg_image;
            $bg_image_path = $this->uploadImageToS3($bg_image);
        }

        // To save Banner
        $banner = array();
        isset($bg_image_path)? $banner['bg_image'] = $bg_image_path : null;
        isset($main_image_path)? $banner['main_image'] = $main_image_path :null;
        isset($request->banner_id)? $banner['id'] = $request->banner_id :null;
        $banner['heading'] = $request->banner_heading;
        $banner['details'] = $request->banner_details;
        $banner['cta_link'] = $request->cta_link;
        $banner['cta_button_label'] = $request->cta_button_label;
        $banner['orientation'] = $request->orientation;

        if(!$request->banner_id) {
            $this->bannerAdd($banner);
            return back()->with([
                'success' => 'Banner added successfully!'
            ])->withInput();
        } else {
            $this->bannerUpdate($banner);
            return redirect()->route('manage-site-landing-page')->with([
                'success' => 'Banner updated successfully!'
            ]);
        }
    }

    public function removeBanner(Request $request) {
        $this->bannerRemove($request);
        return back()->with([
            'success' => 'Banner removed!'
        ]);
    }

    public function updateMainBanner(Request $request) {

        $rules = [
            'heading_1' => 'required',
            'heading_2' => 'required',
            'sub_heading' => 'required',
            // 'video_link' => 'required',
            // 'bg_image' => (!$request->id)? 'required' : '',
            // 'main_image' => (!$request->id)? 'required' : '',
        ];

        $messages = [
            'heading_1.required' => 'Heading 1 is required.',
            'heading_2.required' => 'Heading 2 is required.',
            'sub_heading.required' => 'Sub heading is required.',
            // 'video_link.required' => 'Video link is required.',
            // 'bg_image.required' => 'Background image is required.',
            // 'main_image.required' => 'Main image is required.',
        ];

        $this->validate($request, $rules, $messages);
        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/main-banner';
            $main_image->file = $request->main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // Background Image
        if($request->hasFile('bg_image')) {
            $bg_image = new Collection();
            $bg_image->folder = config('app.aws_bucket_folder').'/main-banner';
            $bg_image->file = $request->bg_image;
            $bg_image_path = $this->uploadImageToS3($bg_image);
        }

        // To save Banner
        $main_banner = array();
        isset($bg_image_path)? $main_banner['bg_image'] = $bg_image_path : null;
        isset($main_image_path)? $main_banner['main_image'] = $main_image_path :null;
        $main_banner['heading_1'] = $request->heading_1;
        $main_banner['heading_2'] = $request->heading_2;
        $main_banner['sub_heading'] = $request->sub_heading;
        $main_banner['video_link'] = $request->video_link;

        $this->mainBannerUpdate($main_banner);

        return back()->with([
            'success' => 'Main banner updated!'
        ])->withInput();
    }

    public function updateSectionProblem(Request $request) {
        $rules = [
            'title' => 'required',
            // 'sub_title' => 'required',
            // 'content' => 'required',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            // 'sub_title.required' => 'Sub title is required.',
            // 'content.required' => 'Content is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/section-problem';
            $main_image->file = $request->main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // Main Image 2
        if($request->hasFile('main_image_2')) {
            $main_image_2 = new Collection();
            $main_image_2->folder = config('app.aws_bucket_folder').'/section-problem';
            $main_image_2->file = $request->main_image_2;
            $main_image_2_path = $this->uploadImageToS3($main_image_2);
        }

        // Main Image 3
        if($request->hasFile('main_image_3')) {
            $main_image_3 = new Collection();
            $main_image_3->folder = config('app.aws_bucket_folder').'/section-problem';
            $main_image_3->file = $request->main_image_3;
            $main_image_3_path = $this->uploadImageToS3($main_image_3);
        }

        // To save Banner
        $sec_problem = array();
        isset($request->video)? $sec_problem['video'] = $request->video : null;
        isset($main_image_path)? $sec_problem['main_image'] = $main_image_path :null;
        isset($main_image_2_path)? $sec_problem['main_image_2'] = $main_image_2_path :null;
        isset($main_image_3_path)? $sec_problem['main_image_3'] = $main_image_3_path :null;
        $sec_problem['title'] = $request->title;
        $sec_problem['sub_title'] = $request->sub_title;
        $sec_problem['content'] = $request->content;
        $sec_problem['content_2'] = $request->content_2;
        $sec_problem['content_3'] = $request->content_3;

        $this->secProblemUpdate($sec_problem);

        return back()->with([
            'success' => 'Section problem updated!'
        ])->withInput();
    }

    public function updateSectionSolution(Request $request) {
        $rules = [
            'title' => 'required',
            // 'sub_title' => 'required',
            // 'content' => 'required',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            // 'sub_title.required' => 'Sub title is required.',
            // 'content.required' => 'Content is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/section-solution';
            $main_image->file = $request->main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // To save Banner
        $sec_solution = array();
        isset($request->video)? $sec_solution['video'] = $request->video : null;
        isset($main_image_path)? $sec_solution['main_image'] = $main_image_path :null;
        $sec_solution['title'] = $request->title;
        $sec_solution['sub_title'] = $request->sub_title;
        $sec_solution['content'] = $request->content;

        $this->secSolutionUpdate($sec_solution);

        return back()->with([
            'success' => 'Section solution updated!'
        ])->withInput();
    }
    
    public function updateSectionFeatures(Request $request) {

        $rules = [
            'title' => 'required',
            // 'sub_title' => 'required',
            // 'main_image' => (!$request->id)? 'required' : '',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            // 'sub_title.required' => 'Sub title is required.',
            // 'main_image.required' => 'Main image is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/section-features';
            $main_image->file = $request->main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // Icon 1
        if($request->hasFile('feat_1_icon')) {
            $feat_1_icon = new Collection();
            $feat_1_icon->folder = config('app.aws_bucket_folder').'/section-features/icons';
            $feat_1_icon->file = $request->feat_1_icon;
            $feat_1_icon = $this->uploadImageToS3($feat_1_icon);
        }

        // Icon 1
        if($request->hasFile('feat_2_icon')) {
            $feat_2_icon = new Collection();
            $feat_2_icon->folder = config('app.aws_bucket_folder').'/section-features/icons';
            $feat_2_icon->file = $request->feat_2_icon;
            $feat_2_icon = $this->uploadImageToS3($feat_2_icon);
        }

        // Icon 1
        if($request->hasFile('feat_3_icon')) {
            $feat_3_icon = new Collection();
            $feat_3_icon->folder = config('app.aws_bucket_folder').'/section-features/icons';
            $feat_3_icon->file = $request->feat_3_icon;
            $feat_3_icon = $this->uploadImageToS3($feat_3_icon);
        }

        // Icon 1
        if($request->hasFile('feat_4_icon')) {
            $feat_4_icon = new Collection();
            $feat_4_icon->folder = config('app.aws_bucket_folder').'/section-features/icons';
            $feat_4_icon->file = $request->feat_4_icon;
            $feat_4_icon = $this->uploadImageToS3($feat_4_icon);
        }

        // Icon 1
        if($request->hasFile('feat_5_icon')) {
            $feat_5_icon = new Collection();
            $feat_5_icon->folder = config('app.aws_bucket_folder').'/section-features/icons';
            $feat_5_icon->file = $request->feat_5_icon;
            $feat_5_icon = $this->uploadImageToS3($feat_5_icon);
        }

        // Icon 1
        if($request->hasFile('feat_6_icon')) {
            $feat_6_icon = new Collection();
            $feat_6_icon->folder = config('app.aws_bucket_folder').'/section-features/icons';
            $feat_6_icon->file = $request->feat_6_icon;
            $feat_6_icon = $this->uploadImageToS3($feat_6_icon);
        }

        // To save Banner
        $sec_features = array();
        $sec_features['title'] = $request->title;
        $sec_features['sub_title'] = $request->sub_title;
        isset($main_image_path)? $sec_features['main_image'] = $main_image_path : null;

        isset($request->feat_1_title)? $sec_features['feat_1_title'] = $request->feat_1_title : null;
        isset($request->feat_1_content)? $sec_features['feat_1_content'] = $request->feat_1_content : null;
        isset($feat_1_icon)? $sec_features['feat_1_icon'] = $feat_1_icon : null;

        isset($request->feat_2_title)? $sec_features['feat_2_title'] = $request->feat_2_title : null;
        isset($request->feat_2_content)? $sec_features['feat_2_content'] = $request->feat_2_content : null;
        isset($feat_2_icon)? $sec_features['feat_2_icon'] = $feat_2_icon : null;

        isset($request->feat_3_title)? $sec_features['feat_3_title'] = $request->feat_3_title : null;
        isset($request->feat_3_content)? $sec_features['feat_3_content'] = $request->feat_3_content : null;
        isset($feat_3_icon)? $sec_features['feat_3_icon'] = $feat_3_icon : null;

        isset($request->feat_4_title)? $sec_features['feat_4_title'] = $request->feat_4_title : null;
        isset($request->feat_4_content)? $sec_features['feat_4_content'] = $request->feat_4_content : null;
        isset($feat_4_icon)? $sec_features['feat_4_icon'] = $feat_4_icon : null;

        isset($request->feat_5_title)? $sec_features['feat_5_title'] = $request->feat_5_title : null;
        isset($request->feat_5_content)? $sec_features['feat_5_content'] = $request->feat_5_content : null;
        isset($feat_5_icon)? $sec_features['feat_5_icon'] = $feat_5_icon : null;

        isset($request->feat_6_title)? $sec_features['feat_6_title'] = $request->feat_6_title : null;
        isset($request->feat_6_content)? $sec_features['feat_6_content'] = $request->feat_6_content : null;
        isset($feat_6_icon)? $sec_features['feat_6_icon'] = $feat_6_icon : null;

        $this->secFeaturesUpdate($sec_features);

        return back()->with([
            'success' => 'Section features updated!'
        ])->withInput();
    }

    public function updateSectionCounter(Request $request) {
        $rules = [
            // 'bg_image' => (!$request->id)? 'required' : '',
            'count_1_title' => 'required',
            // 'count_1_icon' => 'required',
            // 'count_1_value' => 'required',
            // 'count_2_title' => 'required',
            // 'count_2_icon' => 'required',
            // 'count_2_value' => 'required',
            // 'count_3_title' => 'required',
            // 'count_3_icon' => 'required',
            // 'count_3_value' => 'required',
            // 'count_4_title' => 'required',
            // 'count_4_icon' => 'required',
            // 'count_4_value' => 'required',
        ];

        $messages = [
            // 'bg_image.required' => 'Background image is required.',
            'count_1_title.required' => 'Title is required.',
            // 'count_1_icon.required' => 'Icon is required.',
            // 'count_1_value.required' => 'Value is required.',
            // 'count_2_title.required' => 'Title is required.',
            // 'count_2_icon.required' => 'Icon is required.',
            // 'count_2_value.required' => 'Value is required.',
            // 'count_3_title.required' => 'Title is required.',
            // 'count_3_icon.required' => 'Icon is required.',
            // 'count_3_value.required' => 'Value is required.',
            // 'count_4_title.required' => 'Title is required.',
            // 'count_4_icon.required' => 'Icon is required.',
            // 'count_4_value.required' => 'Value is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Background Image
        if($request->hasFile('bg_image')) {
            $bg_image = new Collection();
            $bg_image->folder = config('app.aws_bucket_folder').'/section-counter';
            $bg_image->file = $request->bg_image;
            $bg_image_path = $this->uploadImageToS3($bg_image);
        }

        // To save Counter Section
        $sec_counter = array();
        isset($bg_image_path)? $sec_counter['bg_image'] = $bg_image_path : null;

        $sec_counter['count_1_title'] = $request->count_1_title;
        $sec_counter['count_1_icon'] = $request->count_1_icon;
        $sec_counter['count_1_value'] = $request->count_1_value;

        $sec_counter['count_2_title'] = $request->count_2_title;
        $sec_counter['count_2_icon'] = $request->count_2_icon;
        $sec_counter['count_2_value'] = $request->count_2_value;

        $sec_counter['count_3_title'] = $request->count_3_title;
        $sec_counter['count_3_icon'] = $request->count_3_icon;
        $sec_counter['count_3_value'] = $request->count_3_value;

        $sec_counter['count_4_title'] = $request->count_4_title;
        $sec_counter['count_4_icon'] = $request->count_4_icon;
        $sec_counter['count_4_value'] = $request->count_4_value;


        $this->secCounterUpdate($sec_counter);

        return back()->with([
            'success' => 'Section counter updated!'
        ])->withInput();
    }

    public function updateSectionScreenshot(Request $request) {
        $rules = [
            'title' => 'required',
            // 'sub_title' => 'required',
            // 'main_image' => (!$request->id)? 'required' : '',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            // 'sub_title.required' => 'Sub title is required.',
            // 'main_image.required' => 'Main image is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('main_image')) {
            $main_image = new Collection();
            $main_image->folder = config('app.aws_bucket_folder').'/section-screenshot';
            $main_image->file = $request->main_image;
            $main_image_path = $this->uploadImageToS3($main_image);
        }

        // To save Banner
        $sec_problem = array();
        isset($main_image_path)? $sec_problem['main_image'] = $main_image_path :null;
        $sec_problem['title'] = $request->title;
        $sec_problem['sub_title'] = $request->sub_title;

        $this->secScreenshotUpdate($sec_problem);

        return back()->with([
            'success' => 'Section problem updated!'
        ])->withInput();
    }

    public function addScreenshot(Request $request) {
        $rules = [
            // 'image_alt' => 'required',
            'image' => (!$request->screenshot_id)? 'required' : '',
        ];

        $messages = [
            // 'image_alt.required' => 'Image Alt is required.',
            'image.required' => 'Image is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Main Image
        if($request->hasFile('image')) {
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/section-screenshots/images';
            $image->file = $request->image;
            $image = $this->uploadImageToS3($image);
        }

        // To save Banner
        $screenshot = array();
        $screenshot['image_alt'] = $request->image_alt;
        isset($image)? $screenshot['image'] = $image : null;

        if(!$request->screenshot_id) {
            $this->screenshotAdd($screenshot);
            return back()->with([
                'success' => 'Screenshot added successfully!'
            ])->withInput();
        } else {
            $this->screenshotUpdate($screenshot);
            return back()->with([
                'success' => 'Screenshot updated successfully!'
            ]);
        }
    }

    public function updateSectionDownload(Request $request) {
        $rules = [
            'title' => 'required',
            // 'sub_title' => 'required',
            // 'bg_image' => (!$request->id)? 'required' : '',
            // 'ios_image' => (!$request->id)? 'required' : '',
            // 'ios_link' => (!$request->id)? 'required' : '',
            // 'android_image' => (!$request->id)? 'required' : '',
            // 'android_link' => (!$request->id)? 'required' : '',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            // 'sub_title.required' => 'Sub title is required.',
            // 'bg_image.required' => 'Background image is required.',
            // 'ios_image.required' => 'App Store image is required.',
            // 'ios_link.required' => 'App Store link is required.',
            // 'android_image.required' => 'Play Store image is required.',
            // 'android_link.required' => 'Play Store link is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Background Image
        if($request->hasFile('bg_image')) {
            $bg_image = new Collection();
            $bg_image->folder = config('app.aws_bucket_folder').'/section-download';
            $bg_image->file = $request->bg_image;
            $bg_image_path = $this->uploadImageToS3($bg_image);
        }

        // App Store Image
        if($request->hasFile('ios_image')) {
            $ios_image = new Collection();
            $ios_image->folder = config('app.aws_bucket_folder').'/section-download';
            $ios_image->file = $request->ios_image;
            $ios_image_path = $this->uploadImageToS3($ios_image);
        }

        // Play Store Image
        if($request->hasFile('android_image')) {
            $android_image = new Collection();
            $android_image->folder = config('app.aws_bucket_folder').'/section-download';
            $android_image->file = $request->android_image;
            $android_image_path = $this->uploadImageToS3($android_image);
        }
        // To save Banner
        $sec_download = array();
        isset($bg_image_path)? $sec_download['bg_image'] = $bg_image_path :null;
        isset($ios_image_path)? $sec_download['ios_image'] = $ios_image_path :null;
        isset($android_image_path)? $sec_download['android_image'] = $android_image_path :null;
        $sec_download['title'] = $request->title;
        $sec_download['sub_title'] = $request->sub_title;
        $sec_download['ios_link'] = $request->ios_link;
        $sec_download['android_link'] = $request->android_link;

        $this->secDownloadUpdate($sec_download);

        return back()->with([
            'success' => 'Section download updated!'
        ])->withInput();
    }

    public function updateSectionTestimonial(Request $request) {
        $rules = [
            'title' => 'required',
            // 'sub_title' => 'required',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            // 'sub_title.required' => 'Sub title is required.',
        ];

        $this->validate($request, $rules, $messages);

        // To save Banner
        $sec_testimonial = array();
        $sec_testimonial['title'] = $request->title;
        $sec_testimonial['sub_title'] = $request->sub_title;

        $this->secTestimonialUpdate($sec_testimonial);

        return back()->with([
            'success' => 'Section testimonial updated!'
        ])->withInput();
    }

    public function addTestimonial(Request $request) {
        $rules = [
            'client_name' => 'required',
            'client_said' => 'required',
        ];

        $messages = [
            'client_name.required' => 'Client Name is required.',
            'client_said.required' => 'Content is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Client Image
        if($request->hasFile('client_image')) {
            $client_image = new Collection();
            $client_image->folder = config('app.aws_bucket_folder').'/section-testimonial/client_images';
            $client_image->file = $request->client_image;
            $client_image_path = $this->uploadImageToS3($client_image);
        }

        // To save Banner
        $testimonial = array();
        isset($request->testimonial_id)? $testimonial['id'] = $request->testimonial_id : null;
        $testimonial['client_name'] = $request->client_name;
        $testimonial['client_position'] = $request->client_position;
        $testimonial['client_said'] = $request->client_said;
        isset($client_image_path)? $testimonial['client_image'] = $client_image_path : null;

        if(!$request->testimonial_id) {
            $this->testimonialAdd($testimonial);
            return back()->with([
                'success' => 'Testimonial added successfully!'
            ])->withInput();
        } else {
            $this->testimonialUpdate($testimonial);
            return back()->with([
                'success' => 'Testimonial updated successfully!'
            ]);
        }
    }

    public function removeTestimonial(Request $request) {
        $this->testimonialRemove($request);
        return back()->with([
            'success' => 'Testimonial removed!'
        ]);
    }

    public function removeImageFor(Request $request) {
        switch($request->model) {
            case 'MainBanner':
                $this->removeImageFromMainBanner($request);
                break;
            
            case 'Banner':
                $this->removeImageFromBanner($request);
                break;
            
            case 'SectionProblem':
                $this->removeImageFromSectionProblem($request);
                break;

            case 'SectionSolution':
                $this->removeImageFromSectionSolution($request);
                break;

            case 'SectionFeature':
                $this->removeImageFromSectionFeature($request);
                break;

            case 'SectionCounter':
                $this->removeImageFromSectionCounter($request);
                break;

            case 'SectionScreenshot':
                $this->removeImageFromSectionScreenshot($request);
                break;

            case 'SectionScreenshotImage':
                $this->removeImageFromSectionScreenshotImage($request);
                break;

            case 'SectionDownload':
                $this->removeImageFromSectionDownload($request);
                break;
            
            case 'Page':
                $this->removeImageFromPage($request);
                break;
            
            case 'PartnerPageDetails':
                $this->removeImageFromPartnerPageDetails($request);
                break;
        }

        return back()->with([
            'success' => 'Image removed successfully!'
        ]);
    }

    public function updateSectionContactUs(Request $request) {
        $rules = [
            'title' => 'required',
            'email' => 'required',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            'email.required' => 'Email is required.',
        ];

        $this->validate($request, $rules, $messages);

        // To save Banner
        $sec_contact_us = array();
        $sec_contact_us['title'] = $request->title;
        $sec_contact_us['sub_title'] = $request->sub_title;
        $sec_contact_us['address'] = $request->address;
        $sec_contact_us['contact_number'] = $request->contact_number;
        $sec_contact_us['email'] = $request->email;
        $sec_contact_us['facebook_link'] = $request->facebook_link;
        $sec_contact_us['twitter_link'] = $request->twitter_link;

        $this->secContactUsUpdate($sec_contact_us);

        return back()->with([
            'success' => 'Section contact us updated!'
        ])->withInput();
    }

    public function manageDataIndustry() {
        return view('dashboard.admin.manage-data.industry')->with([
            'industries' => $this->getData('Industry')
        ]);
    }

    public function manageDataSpecialization() {
        return view('dashboard.admin.manage-data.specialization')->with([
            'specializations' => $this->getData('Specialization'),
            'industries' => $this->getData('Industry')
        ]);
    }

    public function manageDataInsurance() {
        return view('dashboard.admin.manage-data.insurance')->with([
            'insurances' => $this->getData('Insurance')
        ]);
    }

    public function actionGet(Request $request) {
        $this->getAction($request);
    }

    public function actionCreate(Request $request) {
        $rules = [
            'name' => 'required',
        ];

        $messages = [
            'name.required' => $request->model . ' name is required.',
        ];

        $this->validate($request, $rules, $messages);

        if(!$request->data_id) {
            $create = $this->createAction($request);

            if($create) {
                return back()->with([
                    'success' => $request->model . ' added successfully!'
                ])->withInput();
            } else {
                return back()->with([
                    'error' => 'Cannot add duplicate name.'
                ])->withInput();
            }
        } else {
            $edit = $this->editAction($request);

            if($edit) {
                return back()->with([
                    'success' => $request->model . ' updated successfully!'
                ])->withInput();
            } else {
                return back()->with([
                    'error' => 'Cannot add duplicate name.'
                ])->withInput();
            }
        }
    }

    public function actionEdit(Request $request) {
        $rules = [
            'name' => 'required',
        ];

        $messages = [
            'name.required' => $request->model . ' name is required.',
        ];

        $this->validate($request, $rules, $messages);

        $edit = $this->editAction($request);

        if($edit) {
            return back()->with([
                'success' => $request->model . ' updated successfully!'
            ])->withInput();
        } else {
            return back()->with([
                'error' => 'Cannot add duplicate name.'
            ])->withInput();
        }
    }

    public function actionRemove(Request $request) {
        $remove = $this->removeAction($request);

        if($remove) {
            return back()->with([
                'success' => $request->model . ' removed successfully!'
            ])->withInput();
        } else {
            return back()->with([
                'error' => 'Error occured while remove item.'
            ])->withInput();
        }
    }

    public function addPartnerPageDetails(Request $request) {
        $rules = [
            'content' => 'required',
        ];

        $messages = [
            'content.required' => 'Content is required.',
        ];

        $this->validate($request, $rules, $messages);

        // Upload main image to AWS S3
        // Client Image
        if($request->hasFile('image')) {
            $image = new Collection();
            $image->folder = config('app.aws_bucket_folder').'/partner-page';
            $image->file = $request->image;
            $image_path = $this->uploadImageToS3($image);
        }

        // To save Banner
        $details = array();
        isset($request->details_id)? $details['id'] = $request->details_id : null;
        $details['content'] = $request->content;
        isset($image_path)? $details['image'] = $image_path : null;

        if(!$request->details_id) {
            $this->partnersPageDetailsAdd($details);
            return back()->with([
                'success' => 'Details added successfully!'
            ])->withInput();
        } else {
            $this->partnersPageDetailsUpdate($details);
            return back()->with([
                'success' => 'Details updated successfully!'
            ]);
        }
    }

    public function addPartnersPageDetailsView() {
        return view('dashboard.admin.manage_site_partners_add');
    }

    public function editPartnersPageDetails($id) {
        return view('dashboard.admin.manage_site_partners_add')->with([
            'details' => $this->getPartnersPageDetailsToEdit($id)
        ]);
    }

    public function removePartnersPageDetails(Request $request) {
        $this->partnersPageDetailsRemove($request);
        return back()->with([
            'success' => 'Details removed!'
        ]);
    }
}
