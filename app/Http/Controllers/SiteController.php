<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Mail;
use App\Mail\NewSubscriber;
use App\Mail\SubscribeAddToList;
use App\Mail\NewContactUs;
use App\Mail\ContactUsSent;
use App\Mail\PartnerSignup;
use App\Mail\PartnerOnboard;
use App\Mail\NotifyNewPartnerSignUp;
use App\Mail\NotifyPartnerOnboarded;
use App\Newsletter;
use App\ContactUs;
use App\Traits\AdminTraits;
use App\Traits\S3Traits;
use App\Page;
use App\SectionContactUs;
use App\Partner;
use App\PartnerDetails;

class SiteController extends Controller
{
    use AdminTraits, S3Traits;
    
    public function landingPage() {
        return view('landing')->with($this->getRequiredData());
    }

    
    public function partnersPage() {
        return view('partners')->with([
            'partners_page_details' => $this->getPartnersPageDetails(),
            'sec_contact_us' => SectionContactUs::first()
        ]);
    }

    public function page(Request $request, string $slug){
        $page = Page::where('slug', $slug)->first();

        if($page) {
            if($slug == 'contact-us') {
                return view('page', [
                    'page' => $page,
                    'sec_contact_us' => SectionContactUs::first()
                ]);
            } else {
                return view('page', ['page' => $page]);
            }
        } else {
            abort(404);
        }
    }

    public function subscribe(Request $request) {
        if($request->email) {
            // Check if already in mailing list
            if(!Newsletter::where('email', $request->email)->first()) {
                Newsletter::create([
                    'email' => $request->email
                ]);
                Mail::to('support@quetap.ph')->send(new NewSubscriber($request));
                Mail::to($request->email)->send(new SubscribeAddToList());
        
                return back()->with([
                    'success' => 'Thank you for subscribing.',
                    'icon' => 'fa-check'
                ]);
            } else {
                return back()->with([
                    'success' => 'Already in the list',
                    'icon' => 'fa-check'
                ]);
            }
        } else {
            return back()->with([
                'success' => 'Please provide your email address.',
                'icon' => 'fa-exclamation'
            ]);
        }
    }

    public function contactUs(Request $request) {
        $rules = [
            'name' => 'required',
            'email' => 'required',
            'message' => 'required',
        ];

        $messages = [
            'title.required' => 'Title is required.',
            'email.required' => 'Email is required.',
            'message.required' => 'Message is required.',
        ];

        $this->validate($request, $rules, $messages);

        Mail::to('support@quetap.ph')->send(new NewContactUs($request));
        Mail::to($request->email)->send(new ContactUsSent($request));

        ContactUs::create($request->all());

        return response()->json([
            'success' => 'Thank you for reaching on us.'
        ]);
    }

    public function partnerSignup()
    {
        return view('auth.register_partner');
    }

    public function partnerSignupPost(Request $request)
    {
        // dd($request->all());

        $rules = [
            'signup_terms' => ['required'],
            'first_name' => ['required', 'string', 'max:191'],
            'last_name' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:partners'],
            'password' => ['required', 'string', 'min:8', 'same:password_confirmation'],
            'contact_no' => ['required'],
            'time_from' => ['required'],
            'time_to' => ['required'],
        ];

        $messages = [
            'signup_terms.required' => 'Agree to Terms and Condition',
        ];

        $this->validate($request, $rules, $messages);

        $request['token'] = Str::random(50).Carbon::now()->timestamp;
        $request['verified'] = 0;
        $request['onboarded'] = 0;

        $partner = Partner::create($request->all());

        $link = config('app.url').'/auth/partner/'.$partner->id.'/onboarding/'.$partner->token;

        Mail::to($request->email)->send(new PartnerSignup($partner, $link));
        Mail::to('email-verification.quetap@quetap.ph')->send(new NotifyNewPartnerSignUp($partner));

        return back()->with([
            'success' => true,
        ]);
    }

    public function partnerOnboarding($id, $token)
    {
        $partner = Partner::where([
            'id' => $id,
            'token' => $token
        ])->first();

        if(!$partner->onboarded) {
            return view('auth.partner_onboarding')->with([
                'id' => $id,
                'token' => $token,
                'partner' => $partner,
                'industries' => $this->getData('Industry'),
                'specializations' => $this->getData('Specialization'),
                'insurances' => $this->getData('Insurance')
            ]);
        } else {
            abort(404);
        }
    }

    public function partnerOnboardingPost(Request $request)
    {
        $partner = Partner::where([
            'id' => $request->partner_id,
            'token' => $request->token,
            'onboarded' => 0
        ])->first();
        
        if($partner) {
            $rules = [
                'business_type' => ['required'],
                'business_name' => ['required', 'string', 'max:191'],
                'representative' => ['required', 'string', 'max:191'],
                'position' => ['required', 'string', 'max:191'],
                'contact_no' => ['required', 'string', 'min:7', 'max:12'],
                'business_address_street' => ['required', 'string', 'max:191'],
                'business_address_province' => ['required', 'string', 'max:191'],
                'business_address_city' => ['required', 'string', 'max:191'],
                'business_address_zip_code' => ['required', 'string', 'max:191'],
                'industry' => ['required', 'string', 'max:191'],
                // 'logo' => ['required', 'max:5000'],
                // 'business_permit' => ['required', 'max:5000'],
                'onboarding_terms' => ['required'],
                'lat_long' => ['required'],
            ];
    
            $messages = [
                'onboarding_terms.required' => 'Authorization required',
                'lat_long.required' => 'Please indicate your exact location'
            ];
    
            $this->validate($request, $rules, $messages);
    
            if($request->hasFile('logo')) {
                $logo = new Collection();
                $logo->folder = config('app.aws_bucket_folder').'/partners'.'/'.$request->partner_id.'/logo';
                $logo->file = $request->logo;
                $logo_path = $this->uploadImageToS3($logo);
            }
    
            if($request->hasFile('business_permit')) {
                $business_permit = new Collection();
                $business_permit->folder = config('app.aws_bucket_folder').'/partners'.'/'.$request->partner_id.'/business-permit';
                $business_permit->file = $request->business_permit;
                $business_permit_path = $this->uploadImageToS3($business_permit);
            }
    
            if($request->hasFile('dti_sec_cert')) {
                $dti_sec_cert = new Collection();
                $dti_sec_cert->folder = config('app.aws_bucket_folder').'/partners'.'/'.$request->partner_id.'/dti-sec-cert';
                $dti_sec_cert->file = $request->dti_sec_cert;
                $dti_sec_cert_path = $this->uploadImageToS3($dti_sec_cert);
            }
    
            if($request->hasFile('bir_cert')) {
                $bir_cert = new Collection();
                $bir_cert->folder = config('app.aws_bucket_folder').'/partners'.'/'.$request->partner_id.'/bir-cert';
                $bir_cert->file = $request->bir_cert;
                $bir_cert_path = $this->uploadImageToS3($bir_cert);
            }

            $details = PartnerDetails::create([
                'partner_id' => $request->partner_id,
                'business_type' => $request->business_type,
                'business_name' => $request->business_name,
                'representative' => $request->representative,
                'position' => $request->position,
                'contact_no' => $request->contact_no,
                'business_address_street' => $request->business_address_street,
                'business_address_province' => $request->business_address_province,
                'business_address_city' => $request->business_address_city,
                'business_address_zip_code' => $request->business_address_zip_code,
                'industry' => $request->industry,
                'specialization' => $request->specialization,
                'insurance' => $request->insurance,
                'logo' => ($request->logo)? $logo_path : 'https://quetap-site-bucket.s3.ap-northeast-1.amazonaws.com/local/partners/7/logo/EkBNru3gUs2pBuYn27keYyPwFPiD3IPjlTIjyy7x.png',
                'business_permit' => ($request->business_permit)? $business_permit_path : 'https://quetap-site-bucket.s3.ap-northeast-1.amazonaws.com/local/partners/8/business-permit/4uFLejDgwSKTsQQnl81sWxSCjvOArBkWc4atMUdk.png',
                'dti_sec_cert' => ($request->dti_sec_cert)? $dti_sec_cert_path : null,
                'bir_cert' => ($request->bir_cert)? $bir_cert_path : null,
                'additional_details' => $request->additional_details,
                'lat_long' => $request->lat_long
            ]);
    
            if($details) {
                $title = 'Account on Review';
                // $message = 'Thank you for sending us your details. We will update you once we verify all your details. You may receive a call from us for our verifcation.';
                $message = 'Partner account to be added in back4app.';

                $partner->update([
                    'onboarded' => 1
                ]);

                // Mail::to($partner->email)->send(new PartnerOnboard($partner, $title, $message));
                Mail::to('email-verification.quetap@quetap.ph')->send(new NotifyPartnerOnboarded($partner));

                return $this->partnerOnboardingSuccess($partner->id, $partner->token, $title, $message);
                // return redirect(route('partners'));
            } else {
                abort(404);
            }

        } else {
            abort(404);
        }
    }

    public function partnerOnboardingSuccess($id, $token, $title, $message) {
        $partner = Partner::where([
            'id' => $id,
            'token' => $token
        ])->first();

        if($partner) {
            return view('auth.success')->with([
                'partner' => $partner,
                'title' => $title,
                'message' => $message
            ]);
        } else {
            abort(404);
        }
    }
}
