<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionFeature extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title',
        'main_image',
        'feat_1_title',
        'feat_1_content',
        'feat_1_icon',
        'feat_2_title',
        'feat_2_content',
        'feat_2_icon',
        'feat_3_title',
        'feat_3_content',
        'feat_3_icon',
        'feat_4_title',
        'feat_4_content',
        'feat_4_icon',
        'feat_5_title',
        'feat_5_content',
        'feat_5_icon',
        'feat_6_title',
        'feat_6_content',
        'feat_6_icon'
    ];
}
