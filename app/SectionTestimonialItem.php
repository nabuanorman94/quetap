<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionTestimonialItem extends Model
{
    protected $fillable = [ 
        'client_name',
        'client_position',
        'client_image',
        'client_said'
    ];
}
