<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionScreenshotImage extends Model
{
    protected $fillable = [ 
        'image',
        'image_alt'
    ];
}
