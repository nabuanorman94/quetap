<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyNewPartnerSignUp extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($partner)
    {
        $this->partner = $partner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Partner Signup')
        ->from(['address' => 'no-reply@quetap.ph', 'name' => 'QueTap'])
        ->markdown('emails.auth.partner.notify-signup')
        ->with([
            'partner' => $this->partner,
        ]);
    }
}
