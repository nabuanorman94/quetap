<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NotifyPartnerOnboarded extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($partner)
    {
        $this->partner = $partner;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Partner Onboarded('. $this->partner->first_name .')')
        ->from(['address' => 'no-reply@quetap.ph', 'name' => 'QueTap'])
        ->markdown('emails.auth.partner.notify-onboarded')
        ->with([
            'partner' => $this->partner
        ]);
    }
}
