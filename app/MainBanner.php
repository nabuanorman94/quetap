<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MainBanner extends Model
{
    protected $fillable = [
        'bg_image',
        'main_image',
        'heading_1',
        'heading_2',
        'sub_heading',
        'video_link'
    ];
}
