<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionContactUs extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title',
        'address',
        'contact_number',
        'email',
        'facebook_link',
        'twitter_link'
    ];
}
