<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Industry extends Model
{
    protected $fillable = [ 
        'name',
    ];

    public function specialization() {
        return $this->hasOne('App\Specialization');
    }
}
