<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionTestimonial extends Model
{
    protected $fillable = [ 
        'title',
        'sub_title'
    ];
}
