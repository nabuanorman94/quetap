<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SectionCounter extends Model
{
    protected $fillable = [ 
        'bg_image',
        'count_1_title',
        'count_1_icon',
        'count_1_value',
        'count_2_title',
        'count_2_icon',
        'count_2_value',
        'count_3_title',
        'count_3_icon',
        'count_3_value',
        'count_4_title',
        'count_4_icon',
        'count_4_value'
    ];
}
