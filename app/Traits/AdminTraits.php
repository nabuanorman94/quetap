<?php 

namespace App\Traits;

use Illuminate\Http\Request;
use App\Page;
use App\Banner;
use App\MainBanner;
use App\SectionProblem;
use App\SectionSolution;
use App\SectionFeature;
use App\SectionCounter;
use App\SectionScreenshot;
use App\SectionScreenshotImage;
use App\SectionDownload;
use App\SectionTestimonial;
use App\SectionTestimonialItem;
use App\SectionContactUs;
use App\Industry;
use App\Specialization;
use App\Insurance;
use App\Partner;
use App\PartnerDetails;
use App\PartnerPageDetails;

trait AdminTraits
{   
    public function getOtherPages() {
        return Page::orderBy('id', 'DESC')->get();
    }

    public function getRequiredData() {
        $data = array();
        $data['banners'] = Banner::orderBy('id', 'DESC')->get();
        $data['main_banner'] = MainBanner::first();
        $data['sec_problem'] = SectionProblem::first();
        $data['sec_solution'] = SectionSolution::first();
        $data['sec_feature'] = SectionFeature::first();
        $data['sec_counter'] = SectionCounter::first();
        $data['sec_screenshot'] = SectionScreenshot::first();
        $data['screenshot_images'] = SectionScreenshotImage::orderBy('id', 'ASC')->get();
        $data['sec_download'] = SectionDownload::first();
        $data['sec_testimonial'] = SectionTestimonial::first();
        $data['testimonial_items'] = SectionTestimonialItem::orderBy('id', 'DESC')->get();
        $data['sec_contact_us'] = SectionContactUs::first();

        return $data;
    }   

    // Other Pages
    public function pageAdd($page) {
        return Page::create($page);
    }

    public function getPageToEdit($id) {
        return Page::where('id', $id)->first();
    }

    public function pageUpdate($page) {
        $my_page = Page::where('id', $page['id'])->first();
        if(isset($page['bg_image'])) {
            $this->removeImageFromS3($my_page->bg_image);
        }
        if(isset($page['main_image'])) {
            $this->removeImageFromS3($my_page->main_image);
        }
        if(isset($page['main_image_2'])) {
            $this->removeImageFromS3($my_page->main_image_2);
        }
        return $my_page->update($page);
    }

    public function pageRemove($request) {
        $my_page = Page::where('id', $request->myid)->first();

        if(isset($my_page->bg_image)) {
            $this->removeImageFromS3($my_page->bg_image);
        }

        if(isset($my_page->main_image)) {
            $this->removeImageFromS3($my_page->main_image);
        }

        if(isset($my_page->main_image_2)) {
            $this->removeImageFromS3($my_page->main_image_2);
        }

        $my_page->delete();
        
        return 'Success';
    }

    // Site Management
    public function bannerAdd($banner) {
        return Banner::create($banner);
    }

    public function bannerUpdate($banner) {
        $my_banner = Banner::find($banner['id'])->first();
        if(isset($banner['bg_image'])) {
            $this->removeImageFromS3($my_banner->bg_image);
        }
        if(isset($banner['main_image'])) {
            $this->removeImageFromS3($my_banner->main_image);
        }
        return $my_banner->update($banner);
    }

    public function bannerRemove($request) {
        $banner = Banner::find($request->myid)->first();
        
        if(isset($banner->bg_image)) {
            $this->removeImageFromS3($banner->bg_image);
        }

        if(isset($banner->main_image)) {
            $this->removeImageFromS3($banner->main_image);
        }

        $banner->delete();
        
        return 'Success';
    }

    public function mainBannerUpdate($banner) {
        if(MainBanner::find(1)) {
            $my_banner = MainBanner::find(1)->first();
            if(isset($banner['bg_image'])) {
                $this->removeImageFromS3($my_banner->bg_image);
            }
            if(isset($banner['main_image'])) {
                $this->removeImageFromS3($my_banner->main_image);
            }
            return MainBanner::find(1)->update($banner);
        } else {
            return MainBanner::create($banner);
        }
    }

    public function secProblemUpdate($sec_problem) {
        if(SectionProblem::find(1)) {
            $my_sec_problem = SectionProblem::find(1)->first();
            if(isset($sec_problem['main_image'])) {
                $this->removeImageFromS3($my_sec_problem->main_image);
            }
            return $my_sec_problem->update($sec_problem);
        } else {
            return SectionProblem::create($sec_problem);
        }
    }

    public function secSolutionUpdate($sec_solution) {
        if(SectionSolution::find(1)) {
            $my_sec_solution = SectionSolution::find(1)->first();
            if(isset($sec_solution['main_image'])) {
                $this->removeImageFromS3($my_sec_solution->main_image);
            }
            return $my_sec_solution->update($sec_solution);
        } else {
            return SectionSolution::create($sec_solution);
        }
    }

    public function secFeaturesUpdate($sec_features) {
        if(SectionFeature::find(1)) {
            $my_sec_features = SectionFeature::find(1)->first();

            if(isset($sec_features['main_image'])) {
                $this->removeImageFromS3($my_sec_features->main_image);
            }

            if(isset($sec_features['feat_1_icon'])) {
                $this->removeImageFromS3($my_sec_features->feat_1_icon);
            }

            if(isset($sec_features['feat_2_icon'])) {
                $this->removeImageFromS3($my_sec_features->feat_2_icon);
            }

            if(isset($sec_features['feat_3_icon'])) {
                $this->removeImageFromS3($my_sec_features->feat_3_icon);
            }

            if(isset($sec_features['feat_4_icon'])) {
                $this->removeImageFromS3($my_sec_features->feat_4_icon);
            }

            if(isset($sec_features['feat_5_icon'])) {
                $this->removeImageFromS3($my_sec_features->feat_5_icon);
            }

            if(isset($sec_features['feat_6_icon'])) {
                $this->removeImageFromS3($my_sec_features->feat_6_icon);
            }

            return $my_sec_features->update($sec_features);
        } else {
            return SectionFeature::create($sec_features);
        }
    }

    public function secCounterUpdate($sec_counter) {
        if(SectionCounter::find(1)) {
            $my_sec_counter = SectionCounter::find(1)->first();
            if(isset($sec_counter['bg_image'])) {
                $this->removeImageFromS3($my_sec_counter->bg_image);
            }
            return $my_sec_counter->update($sec_counter);
        } else {
            return SectionCounter::create($sec_counter);
        }
    }

    public function secScreenshotUpdate($sec_screenshot) {
        if(SectionScreenshot::find(1)) {
            $my_sec_screenshot = SectionScreenshot::find(1)->first();
            if(isset($sec_screenshot['main_image'])) {
                $this->removeImageFromS3($my_sec_screenshot->main_image);
            }
            return $my_sec_screenshot->update($sec_screenshot);
        } else {
            return SectionScreenshot::create($sec_screenshot);
        }
    }

    public function screenshotAdd($screenshot) {
        return SectionScreenshotImage::create($screenshot);
    }

    public function screenshotUpdate($screenshot) {
        $my_screenshot = SectionScreenshotImage::find($screenshot['id'])->first();
        if(isset($screenshot['image'])) {
            $this->removeImageFromS3($my_screenshot->image);
        }
        return $my_screenshot->update($screenshot);
    }

    public function secDownloadUpdate($sec_download) {
        if(SectionDownload::find(1)) {
            $my_sec_download = SectionDownload::find(1)->first();
            if(isset($sec_download['bg_image'])) {
                $this->removeImageFromS3($my_sec_download->bg_image);
            }
            if(isset($sec_download['ios_image'])) {
                $this->removeImageFromS3($my_sec_download->ios_image);
            }
            if(isset($sec_download['android_image'])) {
                $this->removeImageFromS3($my_sec_download->android_image);
            }
            return $my_sec_download->update($sec_download);
        } else {
            return SectionDownload::create($sec_download);
        }
    }

    public function secTestimonialUpdate($sec_testimonial) {
        if(SectionTestimonial::find(1)) {
            return SectionTestimonial::find(1)->update($sec_testimonial);
        } else {
            return SectionTestimonial::create($sec_testimonial);
        }
    }

    public function testimonialAdd($testimonial) {
        return SectionTestimonialItem::create($testimonial);
    }
    
    public function testimonialUpdate($testimonial) {
        return SectionTestimonialItem::where('id', $testimonial['id'])->update($testimonial);
    }
    
    public function testimonialRemove($request) {
        $testimonial = SectionTestimonialItem::where('id', $request->myid)->first();
        if(isset($testimonial->client_image)) {
            $this->removeImageFromS3($testimonial->client_image);
        }

        $testimonial->delete();
        
        return 'Success';
    }

    public function removeImageFromMainBanner($request) {
        
        $main_banner = MainBanner::find(1);
        switch($request->field) {
            case 'main_image':
                $this->removeImageFromS3($main_banner->main_image);
                $main_banner->update([
                    'main_image' => null
                ]);
                break;
            
            case 'bg_image':
                $this->removeImageFromS3($main_banner->bg_image);
                $main_banner->update([
                    'bg_image' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromBanner($request) {
        
        $banner = Banner::where('id', $request->myid)->first();
        switch($request->field) {
            case 'main_image':
                $this->removeImageFromS3($banner->main_image);
                $banner->update([
                    'main_image' => null
                ]);
                break;
            
            case 'bg_image':
                $this->removeImageFromS3($banner->bg_image);
                $banner->update([
                    'bg_image' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromSectionProblem($request) {
        
        $sec_problem = SectionProblem::find(1);
        switch($request->field) {
            case 'main_image':
                $this->removeImageFromS3($sec_problem->main_image);
                $sec_problem->update([
                    'main_image' => null
                ]);
                break;

            case 'main_image_2':
                $this->removeImageFromS3($sec_problem->main_image_2);
                $sec_problem->update([
                    'main_image_2' => null
                ]);
                break;

            case 'main_image_3':
                $this->removeImageFromS3($sec_problem->main_image_3);
                $sec_problem->update([
                    'main_image_3' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromSectionSolution($request) {
        
        $sec_solution = SectionSolution::find(1);
        switch($request->field) {
            case 'main_image':
                $this->removeImageFromS3($sec_solution->main_image);
                $sec_solution->update([
                    'main_image' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromSectionFeature($request) {
        
        $sec_feature = SectionFeature::find(1);
        switch($request->field) {
            case 'main_image':
                $this->removeImageFromS3($sec_feature->main_image);
                $sec_feature->update([
                    'main_image' => null
                ]);
                break;

            case 'feat_1_icon':
                $this->removeImageFromS3($sec_feature->feat_1_icon);
                $sec_feature->update([
                    'feat_1_icon' => null
                ]);
                break;

            case 'feat_2_icon':
                $this->removeImageFromS3($sec_feature->feat_2_icon);
                $sec_feature->update([
                    'feat_2_icon' => null
                ]);
                break;

            case 'feat_3_icon':
                $this->removeImageFromS3($sec_feature->feat_3_icon);
                $sec_feature->update([
                    'feat_3_icon' => null
                ]);
                break;

            case 'feat_4_icon':
                $this->removeImageFromS3($sec_feature->feat_4_icon);
                $sec_feature->update([
                    'feat_4_icon' => null
                ]);
                break;

            case 'feat_5_icon':
                $this->removeImageFromS3($sec_feature->feat_5_icon);
                $sec_feature->update([
                    'feat_5_icon' => null
                ]);
                break;

            case 'feat_6_icon':
                $this->removeImageFromS3($sec_feature->feat_6_icon);
                $sec_feature->update([
                    'feat_6_icon' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromSectionCounter($request) {
        
        $sec_counter = SectionCounter::find(1);
        switch($request->field) {
            case 'bg_image':
                $this->removeImageFromS3($sec_counter->bg_image);
                $sec_counter->update([
                    'bg_image' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromSectionScreenshot($request) {
        
        $sec_screenshot = SectionScreenshot::find(1);
        switch($request->field) {
            case 'main_image':
                $this->removeImageFromS3($sec_screenshot->main_image);
                $sec_screenshot->update([
                    'main_image' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromSectionScreenshotImage($request) {

        $sec_screenshot = SectionScreenshotImage::where('id', $request->myid)->first();
        $this->removeImageFromS3($sec_screenshot->image);
        $sec_screenshot->delete();

        return 'Success';
    }

    public function removeImageFromSectionDownload($request) {
        
        $sec_download = SectionDownload::find(1);
        switch($request->field) {
            case 'bg_image':
                $this->removeImageFromS3($sec_download->bg_image);
                $sec_download->update([
                    'bg_image' => null
                ]);
                break;

            case 'ios_image':
                $this->removeImageFromS3($sec_download->ios_image);
                $sec_download->update([
                    'ios_image' => null
                ]);
                break;

            case 'android_image':
                $this->removeImageFromS3($sec_download->android_image);
                $sec_download->update([
                    'android_image' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromPage($request) {
        $page = Page::where('id', $request->myid)->first();
        switch($request->field) {
            case 'bg_image':
                $this->removeImageFromS3($page->bg_image);
                $page->update([
                    'bg_image' => null
                ]);
                break;

            case 'main_image':
                $this->removeImageFromS3($page->main_image);
                $page->update([
                    'main_image' => null
                ]);
                break;

            case 'main_image_2':
                $this->removeImageFromS3($page->main_image_2);
                $page->update([
                    'main_image_2' => null
                ]);
                break;
        }

        return 'Success';
    }

    public function removeImageFromPartnerPageDetails($request) {
        $page = PartnerPageDetails::where('id', $request->myid)->first();
        switch($request->field) {
            case 'image':
                $this->removeImageFromS3($page->image);
                $page->update([
                    'image' => null
                ]);
                break;
        }

        return 'Success';
    }
    
    public function secContactUsUpdate($sec_contact_us) {
        if(SectionContactUs::find(1)) {
            return SectionContactUs::find(1)->update($sec_contact_us);
        } else {
            return SectionContactUs::create($sec_contact_us);
        }
    }

    public function getData($myModel) {
        $model_name = '\\App\\'.$myModel;
        $model = new $model_name;

        if($myModel == 'Specialization') {
            return $model::orderBy('name', 'ASC')->with('industry')->get();
        } else {
            return $model::orderBy('name', 'ASC')->get();
        }
    }

    public function getPartners() {
        return Partner::orderBy('id', 'DESC')->with('partnerDetails')->get();
    }

    public function getAction($request) {
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;
    }

    public function createAction($request) {
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $exists = $model->where('name', $request->name)->first();

        // Check if name exists
        if(!$exists) {
            $model->create($request->except([
                '_token',
                'data_id',
                'model'
            ]));
    
            return true;
        } else {
            return false;
        }
    }

    public function editAction($request) {
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        $exists = $model->where([
            'name' => $request->name,
            'industry_id' => $request->industry_id
        ])->first();

        // Check if name exists
        if(!$exists) {
            $model->where('id', $request->data_id)
                ->update($request->except([
                '_token',
                'data_id',
                'model'
            ]));
    
            return true;
        } else {
            return false;
        }
    }

    public function removeAction($request) {
        $model_name = '\\App\\'.$request->model;
        $model = new $model_name;

        // Remove data by id
        $model->where('id', $request->id)->delete();

        if($request->model == 'Partner') {
            $details = PartnerDetails::where('partner_id', $request->id)->first();
            if($details) {
                $details->delete();
            }
        }

        return true;
    }

    public function partnersPageDetailsAdd($details) {
        return PartnerPageDetails::create($details);
    }
    
    public function partnersPageDetailsUpdate($details) {
        return PartnerPageDetails::where('id', $details['id'])->update($details);
    }

    public function partnersPageDetailsRemove($request) {
        $details = PartnerPageDetails::where('id', $request->myid)->first();
        if(isset($details->image)) {
            $this->removeImageFromS3($details->image);
        }

        $details->delete();
        
        return 'Success';
    }

    public function getPartnersPageDetailsToEdit($id) {
        return PartnerPageDetails::where('id', $id)->first();
    }

    public function getPartnersPageDetails() {
        return PartnerPageDetails::orderBy('id', 'ASC')->get();
    }
}