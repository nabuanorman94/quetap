<?php 

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

trait S3Traits
{   
    public function uploadImageToS3($image) {
        $path = Storage::disk('s3')->put($image->folder, $image->file, 'public');
        
        return Storage::disk('s3')->url($path);
    }

    public function removeImageFromS3($path) {
        $my_path = str_replace('https://quetap-site-bucket.s3.ap-northeast-1.amazonaws.com/', '', $path);
        return Storage::disk('s3')->delete($my_path);
    }
}