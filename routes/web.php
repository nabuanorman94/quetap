<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::view('/', 'coming_soon');
// Route::view('/landing', 'landing');
Route::get('/', 'SiteController@landingPage')->name('landing-page');
// Route::match(['get', 'post'], '/dashboard', function(){
//     return view('dashboard');
// });

// Quetap Partner Page
Route::get('/become-a-partner', 'SiteController@partnersPage')->name('partners-page');

Route::view('/pages/slick', 'pages.slick');
Route::view('/pages/datatables', 'pages.datatables');
Route::view('/pages/blank', 'pages.blank');

Route::prefix('dashboard')->middleware(['auth', 'for_admin'])->group(function() {
    Route::get('/', 'AdminController@index')->name('admin');

    Route::prefix('manage-site')->group(function() {
        Route::prefix('landing-page')->group(function() {
            Route::get('/', 'AdminController@manageSiteLanding')->name('manage-site-landing-page');

            // Main Banner
            Route::post('/add-main-banner', 'AdminController@updateMainBanner')->name('main-banner-add');

            // Banners
            Route::post('/add-banner', 'AdminController@addBanner')->name('banner-add');
            Route::post('/remove-banner', 'AdminController@removeBanner')->name('banner-remove');

            // Section Problem
            Route::post('/section-problem', 'AdminController@updateSectionProblem')->name('section-problem');

            // Section Solution
            Route::post('/section-solution', 'AdminController@updateSectionSolution')->name('section-solution');

            // Section Features
            Route::post('/section-features', 'AdminController@updateSectionFeatures')->name('section-features');

            // Section Counter
            Route::post('/section-counter', 'AdminController@updateSectionCounter')->name('section-counter');

            // Section Screenshot
            Route::post('/add-screenshot', 'AdminController@addScreenshot')->name('screenshot-add');
            Route::post('/section-screenshot', 'AdminController@updateSectionScreenshot')->name('section-screenshot');

            // Section Download
            Route::post('/section-download', 'AdminController@updateSectionDownload')->name('section-download');

            // Section Testimonial
            Route::post('/add-testimonial', 'AdminController@addTestimonial')->name('testimonial-add');
            Route::post('/remove-testimonial', 'AdminController@removeTestimonial')->name('testimonial-remove');
            Route::post('/section-testimonial', 'AdminController@updateSectionTestimonial')->name('section-testimonial');

            // Remove Image with parameters
            Route::post('/remove-image-for', 'AdminController@removeImageFor')->name('remove-image');

            // Section Contact Us
            Route::post('/section-contact-us', 'AdminController@updateSectionContactUs')->name('section-contact-us');
        });

        // Other Pages
        Route::get('/other-pages', 'AdminController@manageSiteOther')->name('manage-site-other-pages');
        Route::get('/add-page', 'AdminController@addPageView')->name('add-page-view');
        Route::get('/edit-page/{id}', 'AdminController@editPageView')->name('edit-page-view');
        Route::post('/add-page', 'AdminController@addPage')->name('add-page');
        Route::post('/remove-page', 'AdminController@removePage')->name('page-remove');

        // Messages
        Route::get('/messages', 'AdminController@manageSiteMessages')->name('manage-site-messages');

        // Partners Page
        Route::prefix('partners-page')->group(function() {
            Route::get('/', 'AdminController@manageSitePartners')->name('manage-site-partners');
            Route::get('/add-details', 'AdminController@addPartnersPageDetailsView')->name('add-partners-page-details-view');
            Route::get('/edit-details/{id}', 'AdminController@editPartnersPageDetails')->name('edit-partners-page-details');
            Route::post('/add-details', 'AdminController@addPartnerPageDetails')->name('add-partners-page-details');
            Route::post('/remove-details', 'AdminController@removePartnersPageDetails')->name('remove--partners-page-details');
        });
    });

    Route::prefix('manage-data')->group(function() {
        Route::get('/industries', 'AdminController@manageDataIndustry')->name('manage-data-industries');
        Route::get('/specializations', 'AdminController@manageDataSpecialization')->name('manage-data-specializations');
        Route::get('/insurances', 'AdminController@manageDataInsurance')->name('manage-data-insurances');
    });

    Route::get('/users', 'AdminController@users')->name('users');
    Route::get('/partners', 'AdminController@partners')->name('partners');
    Route::get('/establishments', 'AdminController@establishments')->name('establishments');
    Route::get('/advertisements', 'AdminController@advertisements')->name('advertisements');
    Route::get('/newsletters', 'AdminController@newsletters')->name('newsletters');

    Route::prefix('actions')->group(function() {
        Route::post('/get', 'AdminController@actionGet')->name('action-get');
        Route::post('/create', 'AdminController@actionCreate')->name('action-create');
        Route::post('/edit', 'AdminController@actionEdit')->name('action-edit');
        Route::post('/remove', 'AdminController@actionRemove')->name('action-remove');
    });
});

Route::prefix('auth')->group(function() {
    Route::prefix('partner')->group(function() {
        Route::get('/sign-up', 'SiteController@partnerSignup')->name('partners-signup');
        Route::post('/sign-up', 'SiteController@partnerSignupPost')->name('partners-signup-post');

        Route::get('/{id}/onboarding/{token}', 'SiteController@partnerOnboarding')->name('partners-onboarding');
        Route::post('/{id}/onboarding/{token}', 'SiteController@partnerOnboardingPost')->name('partners-onboarding-post');

        Route::get('/{id}/onboarding/{token}/success', 'SiteController@partnerOnboardingSuccess')->name('partners-onboarding-success');
    });
});



// Auth::routes();
Auth::routes([
    'register' => false,
]);

// Route::get('/home', 'HomeController@index')->name('home');
// Newsletter
Route::post('/subscribe', 'SiteController@subscribe')->name('subscribe');
// Contact Us
Route::post('/contact-us', 'SiteController@contactUs')->name('contact-us');

// Other Pages
Route::get('/{page}', 'SiteController@page');

// // Dynamic Pages here
// foreach() {

// }